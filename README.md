<<<<<<< HEAD
Folder for the 3rd project, namely "project3".

This work focuses on a cross-layer design for video transmission in ofdma uplink, which combines video adaptation and resource (allocation) adaptation of OFDMA.

The folder includes LaTex files, simulation results (figures). The simulation code (OMNeT++ project is not included here).

=======
# project3

Cross-layer algorithm to improve QoE for multiple streams in OFDMA multiuser uplink cellular network.

The algorithm jointly considers video adaptation of adpative video streaming with resource adaptation of OFDMA resource allocation.
>>>>>>> 870a525fd21d1868aa773d6de48dabc8eac658bf
