%Analysis

% Video Rate Distortion Curves
    % INPUT
        etha=[33 94.74 128.80 246];
        r0=[0.37 0.35 0.29 0.37];
        d0=[3.61 10.16 12.77 10.80];
        
    %: PLot the curves
        y=zeros(4,600);
        
        Rmin=38;

        for u=1:4
            for i=1:600
                if i>Rmin
                    y(u,i)=10*log10(255*255/(etha(u)/(i*0.01-r0(u))-d0(u)));
                else
                    y(u,i)=0;
                end
            end
        end
        plot(y(1,:)); hold all; grid on;
        plot(y(2,:))
        plot(y(3,:))
        plot(y(4,:))
        xlim([Rmin 600]);