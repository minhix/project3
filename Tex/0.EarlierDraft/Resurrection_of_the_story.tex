\documentclass[]{article}

\usepackage{amsmath,amstext,amsfonts,amssymb}
\usepackage{epsfig,float,subfig}
\usepackage{graphicx}
\usepackage{epstopdf}

\newcommand{\ignore}[1]{}

\begin{document}

\title{Quality-Driven Resource Allocation for Video Streaming in OFDMA Uplink}
\author{Le}
\date{Today}
\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Mobile video traffic is currently accounting for 55 percent of total mobile data traffic and expanding that share to about 72 percent by 2019 \cite{CiscoVNI}. Tremendous demand for mobile video over cellular networks raises huge challenges not only concerning the \textbf{efficiency} in each separate layer (e.g. video compression and spectral efficiency), but also the \textbf{network design}. 

Concerning layers in separation, while adaptive video streaming (AVS) can increase the perceived video quality by dynamically adapting the compression (source) rate to a wide collection of system constraints (e.g. channel conditions and required Quality of Experience), Orthogonal Frequency Division Multiple Access (OFDMA) is favorable for its flexible and efficient spectral usage. In practice, AVS is now adopted in the well-known standard Dynamic Adaptive Streaming over HTTP (DASH) \cite{Sullivan2012}, and OFDMA is deployed in WiMAX and LTE-A.

On the other hand, the concept of cross-layer design for video services has been attracted enormous attention in the last decade. One of the main reason is there are vast opportunities to optimize together the flexibility provided by AVS and OFDMA in order to further enhance the perceived video quality.

A great number of works has been conducted with the goal to improve the video quality by designing appropriate cross layer algorithms, however there are two major limitations of the literature on cross-layer video optimization, which lies in the video quality assessment and the time-scale problem.


%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%
\subsection{Video Stream}
%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%

\begin{figure}[h]
\centering
\includegraphics[width=0.35\textwidth]{../Figures/Illustrations/GOP.eps}
\caption{Illustration of GOP}
\label{fig:GOP}
\end{figure}


\begin{itemize}
\item Video stream consists of the video transmission and the simultaneous video playback.
\item A video stream can be seen as a series of Group of Pictures (GOP). $T_{GOP}$ denotes the GOP duration, which takes value of few hundred milliseconds (e.g. 500ms).
\item To measure the perceived video quality, we use the most common objective quality model based on Peak Signal to Noise Ratio (PSNR). 
\item The perceived quality is computed per GOP. We denote the perceived quality of user $m$ in GOP $l$ by $Q_{m}^{l}$, which takes a real positive value. Since the PSNR value $Q_{m}^{l}$ is the measurement in a specific GOP, we refer to it as \textbf{the short-term video quality}.

\begin{figure}[h]
\centering
\includegraphics[width=0.35\textwidth]{../Figures/Illustrations/QualityPSNR.eps}
\caption{Illustration of user's video sequence with short-term video quality $Q_m^l$}
\label{fig:SeqPSNR}
\end{figure}

\item General, $Q_{m}^{l}$ is a exponentially increasing function of the total amount of bytes $B_{m}^{l}$ user $m$ sends during GOP $l$. We have
\begin{equation}
Q_m^l= G_m^l(B_m^l)
\end{equation}
It is important to note that function $G()$ depends on the characteristics of the video. For example, let us assume that the video sent by user $m_2$ contains more motion than the one of user $m_1$. Then, to achieve a similar perceived quality as the one of user $m_1$, user $m_2$ needs to send more data as illustrated in Figure \ref{fig:functionG}, in other words, it requires higher bitrate $R_m^l$ where $R_m^l = B_m^l / T_{GOP}$.
Basically, $G_m^{l}()$ is different for different GOP, however for simplicity, we assume it is the same for all GOP $G_m() \equiv G_m^l(), \forall l$.
\begin{figure}[h]
\centering
\includegraphics[width=0.35\textwidth]{../Figures/Illustrations/functionG.eps}
\caption{Illustration of function $G(.)$}
\label{fig:functionG}
\end{figure}

\item \textbf{Adaptive Video Streaming (AVS)} is the concept, where video encoder can adjust several encoding parameters in order to adapt the bitrate to the network conditions. In other words, one can adjust the $R_m^l$ (i.e. $B_m^l$) and thus the $Q_m^l$ to the available bandwidth or other constraints (e.g. expected perceived quality or latency).
AVS is practical by using the Scalable Video Coding technique (SVC), which is now included in newer video encoding standards such as High Efficient Video Coding (HEVC) and its proceeder H.264/AVC with SVC. AVS can be now found in the well know Dynamic Adaptive Video over HTTP (DASH).
\end{itemize}

%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%
\subsection{Time Scale Problem}
%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%

\begin{itemize}
\item The decision for resource allocation (at MAC) takes place at the pace of the channel coherence time in order to exploit the frequency and multi-user diversities. And channel coherence time in cellular networks typically takes a value of few milliseconds (e.g. 5ms). We set the coherence time equal 5ms, which is equal one OFDMA frame in WiMAX.

\item Time-scale Problem: the decision for encoding (at Application layer) takes place at the scale of GOP duration (i.e. few hundred milliseconds like 500ms), which is much longer than decision for resource allocation.

\item We denote the number of OFDMA frames in each GOP by $K_m$ for user $m$. During each uplink frame $k$ in GOP $l$, user $m$ can be assigned some resource unit to send $b_m^{k,l}$ [bytes]. Obviously, we have
\begin{equation}
B_m^l  = \sum_{k=0}^{K_m-1} b_m^{k,l} \qquad and \qquad Q_m^l=G_m^l(\sum_{k=0}^{K_m-1} b_m^{k,l})
\end{equation}

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{../Figures/Illustrations/TimeScaleProblem_Full2.eps}
\caption{Illustration of the time-scale problem}
\label{fig:TimeScaleProblem}
\end{figure}

\end{itemize}

%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%
\subsection{Quality of Experience}
%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%

\begin{itemize}
\item Quality of Experience (QoE) is the primary criteria to design any video systems.
\item QoE depends on several factors associated with human psychological and biological systems, thus QoE is subjective and difficult to model.
\item Some important affecting QoE: (1) \underline{in short-term:} resolution of image frame and image frame latency and jitter, (2) \underline{in long-term:} video continuity and quality variations.
\item It is proved that the short-term-quality variation between GOPs can significantly reduce the QoE, even with a relatively high average PSNR \cite{Yim2011}.
\item On the other hand, roughly speaking, Quality of Service (QoS), which can be defined based on packet loss rate or latency, is indirect and inefficient way to address QoE.
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Goal of the work}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure}[h]
\centering
\includegraphics[width=0.25\textwidth]{../Figures/Illustrations/UplinkCell.jpg}
\caption{Multiple users streaming video in OFDMA uplink}
\label{fig:upcell}
\end{figure}

We consider a single cell in separation, where $M$ users are streaming videos simultaneously in uplink.

We address the problem of improving QoE for multiple video streams in OFDMA uplink. Practically, we consider the following aspects to improve QoE:

\begin{itemize}
\item \textbf{Short-term quality:} We increase short-term quality $Q_m^l$ for all individual user's GOP.
\item \textbf{Long-term quality:} We reduce the variation of short-term qualities between adjacent GOPs.
\item \textbf{Fairness:} Besides, a quality fairness is also taken into account.
\end{itemize}

Such aspects can be represented by the following \textbf{max-min weighted quality optimization problem} (OP)
\begin{equation}
\centering
\begin{split}
&max \quad \Phi \\
s.t. \quad & \frac{Q_m^{l_m}}{\bar{Q}_m^{l_m}} \geq \Phi \qquad , \forall m
\end{split}
\label{eq:longtermOP}
\end{equation}
where $\bar{Q}_m^l$ is the PSNR average of user $m$ over the last $L$ GOPs. Through this normalization, we heuristically reduce the short-term-quality variation between GOPs. We have
\begin{equation}
\bar{Q}_m^{l_m} = \frac{1}{L} \sum_{i=1}^{L} Q_{m}^{l_{m}-i}
\end{equation}
%and
%\begin{equation}
%Var_m^{l_m} = \frac{1}{L} \sum_{i=1}^{L} \big( Q_{m}^{l_{m}-i} - \bar{Q}_m^{l_m}\big)^2
%\end{equation}
%Then we aim to
%\begin{equation}
%max \quad min \quad \frac{Q_{m}^{l_m}}{Mean^{user_m}}
%\end{equation}
%
%\begin{equation}
%max \quad \sum_{m=1}^{M} (Mean^{user_m}- \eta \times Variance^{user_m})
%\end{equation}
%
%\begin{equation}
%max \sum_{m=1}^{M} (\bar{Q}_m^{l_m} - \eta Var_m^{l_m})
%\end{equation}
%
%\begin{equation}
%\max_{(Q)} \qquad \sum_{m=1}^{M} (\frac{1}{L+1}
%\mathbf{Q_m^{l_m}}+\frac{L}{L+1}\bar{Q}_m^{l_m} - \eta Var_m^{l_m})
%\end{equation}
%
%\begin{equation}
%Q_m^{l} = 10 \log_{10}(\frac{255^2}{MSE})
%\end{equation}
%
%where $\mathbf{Q}=(Q_m^{l_m}, 1< m <M)$ and

We also can write the above formulation as follows:
\begin{equation}
\centering
\begin{split}
&max \quad \Phi \\
%s.t. & \frac{G_m(B_m^l)}{\bar{Q}_m^l} = \frac{1}{\bar{Q}_m^l} G_m(\sum_{k=0}^{K_m} b_m^{k,l)}) \geq \Phi \qquad , \forall m
s.t. \quad & \frac{1}{\bar{Q}_m^{l_m}} G_m(\sum_{k=0}^{K_m} b_m^{k,l)}) \geq \Phi \qquad , \forall m
\end{split}
\label{eq:longtermOP}
\end{equation}

Therefore, if the channel for all OFDMA uplink frame $k$ in the GOP $l$  is known, one might formulate the appropriate optimization problem and find the optimal resource allocation in MAC in order to improve QoE with subject to the aforementioned aspects. However, since the channel is unknown at the beginning of the GOP, this max-min weighted goal needs to be pursued sequentially at each uplink frame of the GOP $l$.

It means we have to formulate and solve the following optimization problem during each uplink frame $k_{m}^*$ of the current GOP $l$.
\begin{equation}
\centering
\begin{split}
&max \quad \Phi \\
s.t. & \frac{1}{\bar{Q}_m^{l_m}} g_m(\sum_{k=0}^{k<k_{m}^*<K_m} b_m^{k,l_m)}) \geq \Phi \qquad , \forall m
\end{split}
\label{eq:longtermOP}
\end{equation}

where $g_m()$ is derived from $G_m()$ and used to compute the intermediate perceived quality of current GOP at the current uplink frame $k_{m}^*$ in GOP $l$. We discuss about this coupling function in next sections. $B_{m}^{l_m}$

For simplicity, we denote the accumulated bits sent until uplink frame $k_{m}^*$ in current GOP $l$ by user $m$ is $\tilde{b}_{m}^{k_{m}^*,l}$
\begin{equation}
\tilde{b}_{m}^{k_{m}^*,l_m}=\sum_{k=0}^{k<k_{m}^*<K_m} b_m^{k,l_m}
\end{equation}
We then denote the intermediate perceived quality by $\tilde{q}_m^{k_{m}^*,l_m}=g(\tilde{b}_{m}^{k_{m}^*,l}) b_m^{k_{m}^*,l_m}$.

Figure \ref{fig:OP_Levels} illustrates the infeasible optimization at level of GOPs (due to time-scale problem), and practical optimization at level of each uplink frame in GOP.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{../Figures/Illustrations/OP_Levels.eps}
\caption{Illustration of infeasible optimization at level of GOPs, and practical optimization at level of each uplink frame in GOP}
\label{fig:OP_Levels}
\end{figure}

It can be easily seen that, when there is no data exchange and cooperation between resource allocation and video encoding, it is most likely that 
\begin{itemize}
\item Through a series of separate resource allocation cycles, each of which take place for one uplink frame, users might get inappropriate amount of resource, which can help to improve the QoE as discussed above.
\item The diversity of video resource, where users needs different amount of resource for a similar perceived quality, cannot be exploited.
\end{itemize}

\textbf{Therefore, a cross-layer design, where video encoding and resource allocation can share information and cooperate is crucial.}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Novel Cross-layer Design}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 The great demand for video streaming gives rise to the need for not only (1) efficiency in each layers, but also (2) good network design. Regarding the network design, cross-layer design has emerged as one of the common solution for the future systems.

\subsection{State-of-art}

In order to improve QoE of video streaming, several works in literature have introduce cross-layer designs combining OFDMA for its flexible and efficient spectral usage and Adaptive Video Streaming (AVS) for its ability to match the bitrate to the network conditions. However, there are two main limitations in the related works.

\begin{itemize}
\item Since the QoE is difficult to model and dealt with, a large amount of works still relies on QoS-based matrix (e.g. latency or packet loss rate). Furthermore, most of the rest, although target QoE, but focus only in short-term video perceived quality. The temporal quality variation is not properly investigated. [Ref will be added later]
\item To alleviate the complexity, most of the work assume that the decision for video encoding and decision for resource allocation take place at the same duration. This assumption thus strongly limit the contribution.
\end{itemize}

\subsection{Novel Cross-layer Design}

\begin{figure}[h]
\centering
\includegraphics[width=0.6\textwidth]{../Figures/Illustrations/CrossLayer_SignalingFlow.eps}
\caption{Illustration of the novel cross-layer design}
\label{fig:CrossLayerDesign}
\end{figure}


\begin{itemize}
\item In this work, we envision a novel cross-layer design combining Adaptive Video Streaming and OFDMA, where the characteristics of user's videos along with the channel conditions are sent to the central cross-layer optimization.
\item In general, the cross-layer optimization aims to improve the user's QoE, which includes short-term quality, long-term quality and quality fairness.
\item To deal with the time-scale problem, during each uplink frame, based on the channel and knowledge about user's videos, the cross-layer optimization is to maximize the minimum weighted intermediate quality.
\item The resource allocation at the output can (1) exploit the frequency and multi-user diversities to improve the spectral usage and, at the same time, (2) assign resource to users in order to efficiently increase the min weighted intermediate quality.
To do that, we adopt the approach introduced in our previous paper \cite{Le2014}. In that work, to improve the spectral usage, we suggest to use shortened overhead and optimize the resource allocation to max-min user throughput. We will extend that approach later to integrate the QoE goal.
\end{itemize}

\subsection{Coupling funciton}

We have
\begin{equation}
\bar{q}_m^{k^*,l_m} = g_m(\tilde{b}_m^{k^*,l_m}) = \tilde{b}_m^{k^*-1,l_m} = Q_m^{l_m}
\end{equation}
then
\begin{equation}
\bar{q}_m^{k_m^*,l_m} = \bar{q}_m^{k_{m}^*-1,l_m} + \alpha_m b_m^{k_{m}^*,l_m}
\end{equation}
where
\begin{equation}
\alpha_m^{l_m} = \frac{10}{ln(10)} \frac{1}{(\theta_m^{l_m} + D0_m^{l_m}	(\tilde{b}_m^{k_{m}^*-1,l_m}/T_{GOP}^{m} - R0_m^{l_m}))(\tilde{b}_m^{k_{m}^*-1,l_m}/T_{GOP}^{m}- R0_m^{l_m})}
\end{equation}

\subsection{Max-min weighted intermediate quality}

Basically, we have
\begin{equation}
\centering
\begin{split}
&max \quad \Phi \\
s.t. & \frac{1}{\bar{Q}_m^l} g_m(\sum_{k=0}^{k_{m}^***<K_m} b_m^{k,l)}) \geq \Phi \qquad , \forall m
\end{split}
\label{eq:longtermOP}
\end{equation}

then

\begin{equation}
\centering
\begin{split}
&max \quad \Phi \\
s.t. \quad & a) \frac{1}{\bar{Q}_m^{l_m}} \big( \bar{q}_m^{k_{m}^*-1,l_m} + \alpha_m^{l_m} b_m^{k_{m}^*,{l_m}}\big) \geq \Phi \qquad , \forall m \\
& b) b_m^{k,l_m} = \sum_{i=0}^{N_{sch}-1} x_{i,m} F(\bar{\gamma}_{i,m},P_{err}) \\
& c) \sum_{m=0}^{M-1} x_{i,m} \leq 1 \qquad , \forall m
\end{split}
\label{eq:longtermOP}
\end{equation}


Furthermore, we adopt the approach from previous paper and we derive
\begin{equation}
\gamma_{i,m}=\frac{P_{i,m} H_{i,m}}{\sigma^2 + MAI_{i,m}} \quad ,
\label{eq:schSINR}
\end{equation}

%\begin{equation}
%\bar{\gamma}_{i,m}=\frac{P_{i,m}^{TX} \times H_{i,m}}{\sigma^2 + \sum_{\forall m' \neq u}\sum_{\forall j \neq i}\overline{MAI}_{i,m}^{j,m'}} \quad ,
%\label{eq:schSINR}
%\end{equation}
\begin{equation}
\begin{split}
& \hspace{20pt} max \quad \Phi \\
s .t. \quad & a) \frac{1}{\bar{Q}_m^{l_m}} \big( \bar{q}_m^{k_{m}^*-1,l_m} + \alpha_m^{l_m} b_m^{k_{m}^*,l_m}\big) \geq \Phi \\
& b) b_m^{k_{m}^*,l_m} \leq \sum_{s=0}^{S} z_{i,m,s} C_{s} \quad, \forall i,m \\
& c) \sum_{s=0}^{S} z_{i,m,s} \widetilde{Th}_{i,m,s} \geq \big( \sum_{j} \sum_{m'} \frac{\overline{MAI}_{i,m}^{j,m'}}{\sigma^2} x_{j,m'} \big) \quad , \forall i,m \\
& d) \sum_{s=0}^{S} z_{i,m,s} = 1 \quad , \\
& e) \sum_{m=0}^{M-1} x_{i,m} \leq 1 \quad , \forall i,m
\end{split}
\label{eq:OP1.2}
\end{equation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bibliographystyle{IEEEtran}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{thebibliography}{1}

\bibitem{Yim2011}
C. Yim and A. C. Bovik. \textit{``Evaluation of temporal variation of video quality in packet loss networks"}. Image Communications, 2011.

\bibitem{Le2014}
Hieu Le, Arash Behboodi, Adam Wolisz, \textit{``Dynamic Resource Allocation in OFDMA Uplink for MAI Mitigation and Throughput Improvement,''} IEEE Vehicular Technology Conference (VTC), 2014.

\end{thebibliography}
\end{document}