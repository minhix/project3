%!TEX root = techreport.tex

\chapter{System Model}
\label{chap:SystemModel}

We introduce in this chapter the system model used in this work, which essentially consists of four parts. First, we describe (1) OFDMA resource allocation in MAC and Physical layers followed by (2) the adaptive video streaming system. Then we show (3) the general structure for the cross-layer system. Finally we discuss about (4) the fundamental challenge in cross layer systems, which is the time scale problem.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{OFDMA system} \label{sec:OFDMA}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
We consider one cell in isolation using OFDMA in uplink, where $M$ users share total $N_{sca}$ orthogonal subcarriers to stream their videos in uplink. The duplex method is Time Devision Duplexing (TDD). 

\begin{figure}[h]
\centering
\includegraphics[width=0.25\textwidth]{../../Figures/Illustrations/UplinkCell.jpg}
\caption{Illustration of a single cell under consideration}
\label{fig:SingleCell}
\end{figure}

Let $f_0$ be the frequency spacing between every two adjacent subcarriers, then the OFDMA symbol (shortly symbol) duration in time equals $T_{sym}=1/f_0$. Prior to each symbol, a Cyclic Prefix (CP) with the length of $v$ is inserted. There are total $N_{sym}$ symbols in each uplink frame. 

The smallest addressable OFDMA resource unit is one subchannel, which takes $G$ consecutive subcarriers in frequency, and $N_{sym}$ symbols in time (i.e. the whole uplink frame). In this work, $G$ has the value equal to the coherence bandwidth, hence one can reduce the overhead and still exploit the frequency diversity \cite{Le2014}. We denote the total number of available subchannel by $N_{sch}$, and $N_{sch}=N_{sca}/G$.

Before sending an uplink frames, a resource allocation scheme, which informs users resource partitions assigned to them, needs to be defined, normally, by BS. To do that, BS can either adopt static schemes (e.g. Blockwise and Interleaving schemes) or dynamically adapt the assignment to several factors such as channel conditions and user's demands. The process adapting the resource allocation scheme to system conditions is referred in this work as resource allocation adaptation or, shortly, \textbf{resource adaptation}. Based on resource allocation scheme, each user modulates their subchannels to stream their videos in uplink to BS.

User signals arriving at BS degrade due to path-loss, shadowing loss and multipath loss. We assume a slow-fading channel in uplink, hence channel behaves similarly in the coherence time span, which equal a few hundred milliseconds \cite{Rappaport2001}. For simplicity, the coherence time of channel in this work assumably takes value equal one uplink frame. (An uplink frame in WiMAX has a duration of 500ms). Let $H_{i,m}^t$ then represent the average channel gain that user $m$ experiences on subchannel $i$ during the uplink frame $t$.

In general, the resource adaptation needs to be conducted for each coherence time span in order to exploit the multi-user and frequency diversities. Then resource adaptation takes place in this work for every OFDMA uplink frames.

It is important to mention that our system model makes the following assumptions. 
\begin{itemize}
\item First, during the downlink, in order to receive correctly the signals sent from BS, users need to estimate channel quality and offsets. Users then lock their clock to the BS's one to reduce the offsets.

Then, we assume these estimations are sent to BS. Based on those estimations and a rough estimation conducted at the beginning of the uplink frame, BS can efficiently estimate the channel quality as well as time and frequency offsets in uplink. In other words, BS has the perfect knowledge of the channel conditions.

\item Signaling is assumably sent on a separate channel and overhead information is not considered. 

\item We also assume an equal and static transmission power on all subcarrier. 

\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Multiple Access Interference} \label{sec:MAI}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In uplink of the multiple user OFDMA systems, each user modulates modulates several subcarriers, and then sends its data to Base Station (BS). The signal arriving at BS is actually constituted by several overlapping individual signals. Due to several reasons such as clock errors, propagation delay, Doppler shift, signals arriving at BS are most probably asynchronous \cite{Tonello2000}. Consequently, the asynchronicity damages the orthogonality and gives rise to Multiple Access Interference (MAI). MAI is considered as one of the fundamental challenge to deploy OFDMA in uplink. We describes the problem of MAI in depth in this section.

\begin{figure}[h]
\centering
\includegraphics[width=0.4\textwidth]{../../Figures/Illustrations/Resource_WithOffset.jpg}
\caption{Illustration of imperfect synchronization}
\label{fig:Offset}
\end{figure}

In general, the Signal to Noise plus Interference Ratio (SINR) $\gamma_{i,m}^{t,l_m}$ can be computed as follows:

\begin{equation}
\gamma_{i,m}^{t,l_m}=\frac{P_{i,m}^{TX} H_{i,m}^{t,l_m}}{\sigma^2 + \overline{MAI}_{i,m}}=\frac{P_{i,m}^{RX}}{\sigma^2 + \sum_{\forall j \neq i,\forall m' \neq m}\overline{MAI}_{i,m}^{j,m'}}
\label{eq:SINR}
\end{equation}

where $P^{TX}_{i,m}$ and $P^{RX}_{i,m}$ are the transmit and the received power, $\sigma^2$ denotes the thermal noise power. The mean MAI (over all subcarriers $r$ in $i$) $\overline{MAI}_{i,m}^{j,m'}$ caused by all subcarriers $r'$ in $j$ of $m'$ takes the following form:

\begin{equation}
\overline{MAI}_{r,u}^{r',u'} = \frac{1}{G} \sum_{r} \sum_{r'} MAI_{r,u}^{r,'u'}
\label{eq:avgMAI}
\end{equation}

where $MAI_{r,m}^{r',m'}$ represents the MAI caused by subcarrier $r'$ of subchannel $j'$ of user $m'$ on subcarrier $r$ of subchannel $i$ of user $m$. The detailed calculation of $MAI_{r,m}^{r',m'}$ can be found in\cite{Tonello2000}, here we shows only the main part needed for the further discussion. Let the frequency and time offsets between user $m$ and user $m'$ are $\Delta f$ and $\tau$, respectively.
Then, $MAI_{r,m}^{r',m'}$ is computed as follows
\begin{equation}
MAI_{r,m}^{r',m'} = \frac{ P_{r',m'}^{RX} }{N_{sca}^{2}} \times \frac{A(r-r', \Delta f, \tau)}{sin^{2}[\frac{\pi}{N_{sca}}(r-r'+\Delta f)]}
\label{eq:calcMAI}
\end{equation}

We adopt Adaptive Coding and Modulation (ACM) mechanism to come up with a more realistic system model. Consequently, based on the instantaneous $\gamma_{i,m}^{t,l}$, user $m$ chooses an appropriate coding and modulation scheme to send data on subchannel $i$ at frame/time $k$ subject to a predefine tolerable error rate $P_{err}$. Let $S$ denote the total number of ACM schemes that users can use.

\begin{figure}[ht]
\centering
\includegraphics[width=0.33\textwidth]{../../Figures/Illustrations/SINR_Bit_simple.jpg}
\caption{Adaptive Coding and Modulation function $F()$}
\label{fig:ACM}
\end{figure}

The ACM scheme selection is illustrated in Figure \ref{fig:ACM}. Then, we uses function $F(.)$ to represent ACM schemes by returning the number of bits $b_{i,m}$, that user $m$ sends on subchannel $i$ corresponding to SINR $\gamma_{i,m}$, i.e. $b_{i,m} = F(\gamma_{i,m}, P_{err})$. As it shows the step function $F()$ is non-continuous, thus non-convex.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{MAI Mitigation and Dynamic Resource Allocation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\subsubsection{Estimation and Correction Based Approach}
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The conventional approach to deal with MAI is to estimate and then correct all offsets, thus it is referred as Estimation and Correction Based Approach (ECBA). However, this approach requires big overhead for offsets estimation. As an example, Figure \ref{fig:Offset} illustrates a PUSC tile, which is the smallest addressable resource unit in the 802.16e WiMAX standard. As it shows, one third of the resource is used for the pilot carriers. Moreover, due to the high complexity of the correction algorithm, and imperfect implementation, there is always chance to have residual offsets \cite{Hwang2009}. Therefore, ECBA is suboptimal. And as shown in \cite{Le2014}, even a small residual offsets can significantly deteriorate user throughput.

\begin{figure}[h]
\centering
\includegraphics[width=0.23\textwidth]{../../Figures/Illustrations/Tile_PUSC.jpg}
\caption{Illustration of a PUSC tile in WiMAX, where overhead takes 33\% resource}
\label{fig:overhead}
\end{figure}
 
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\subsubsection{Dynamic Resource Allocation}
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

An alternative approach to deal with MAI is based on the approach of Dynamic Resource Allocation.

As it shows in Section \ref{sec:MAI}, MAI increases proportionally to the received power on the interfering subcarrier (i.e. $P_{r',m'}^{RX}$). Therefore, resource allocation based on mere channel quality without consideration of the imperfect synchronization can lead to insufficient solution \cite{Le2014}. For example, assigning subcarrier $r'$ to user $m'$, whose channel quality on $r'$ is relatively good, then (1) increases $P_{r',m'}^{RX}$ thus throughput of user $m'$, but (2) also increases $MAI_{r,m}^{r',m'}$ and reduces throughput of user $m$.

On the other hand, MAI also depends on several other factors including not only the time and frequency offsets, but also the distance in frequency between interfering and interfered subchannels (i.e. $(r-r')$).

This remark then shows the potential to mitigate MAI via an appropriate resource allocation. Moreover, resource adaption can, at the same time, exploit the multiuser and frequency diversities to improve the spectral efficiency and better meet user's demand.

In this work, we use a system based on dynamic resource allocation similar to the one introduced in \cite{Le2014}. The system under consideration has the following features:
\begin{itemize}
\item First, the overhead for estimation and correction purposes is shortened. Particularly, the CP is long enough to cope with delay spread caused by multipath transmission, and the pilot subcarriers used for tracking the residual offsets and minor changes of channel conditions are converted to convey user data.

Note that the channel coherence time of cellular networks typically takes a value in range of few milliseconds (as long as a uplink frame), thus the channel does not change dramatically during one uplink frame. Consequently, it is reasonable to not estimate the channel changes during the uplink frame.

\item MAI caused by time offsets (including propagation delay and clock errors in time) and frequency offsets (including Doppler shift and clock errors in frequency) is mitigated by optimizing resource allocation.

\end{itemize}

The fundamental question then becomes: can the gain by exploiting the multiuser and frequency diversities exceed the loss caused by the residual offsets. Fortunately, as it shows in \cite{Le2014}, by formulating and solving an appropriate optimization problem, mechanism of dynamic resource allocation is possible to balance the aforementioned trade-off and increase the minimum user throughput. In this work, we would then extend the flexibility of resource allocation to consider the video quality.

%
%\subsubsection{Optimization Problem}
%
%We then introduce the general optimization problem of the resource allocation. 

%Then, resource allocation is optimized in order to mitigate MAI (caused by untreated offsets) and, at the same time, maximize the minimum user throughput (i.e. max-min fairness). In other words, the spectral efficiency is increased by exploiting the frequency and multi user diversities.
%
%Moreover, we see the potential to further extend the flexibility of resource allocation to improve the more complex goals in higher layer such as QoE.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Video Delivery} \label{sec:VideoDelivery}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

We consider the delivery of videos streamed by users in uplink. Video streams are sent to the Base Station (BS) before passed on to the final destination (over backbone communication). In this work, we focus on the problem of resource adaptation for the links between users and BS, thus we assume the streams received at BS and at final receivers have exactly the same quality.

A coded video stream can be seen as a sequence of Group of Pictures (GOPs), each of which in turn consists of a series of successive image frames (or shortly as image, to distinguish with OFDMA physical frame). Let $L_m$ be the number of GOPs in the video sequence of user $m$, and $T_m^{GOP}$ be the duration of it's GOP. In general, the longer the duration $T_m^{GOP}$, the more efficient the compression is, but the higher the latency. In practice, $T_m^{GOP}$ takes a value of few hundred milliseconds.

%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%
\subsection{Adaptive Video Streaming}
\label{sec:AVS}
%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%

%NOTE: video encoding -> video streaming -> adaptive video streaming -> quality -> adaptation.

Video coding standards (e.g. H.264/Advanced Video Coding with Scalable Video Coding extension (H.264/AVC with SVC) and High Efficient Video Coding (HEVC)) can encode a video input into a scalable video stream, which is comprised of normally several hierarchical video sub-streams. Essentially, the video stream is scalable as parts of the it (i.e. some \textit{"higher"} sub-streams) can be discarded, the remaining sub-streams form another valid video streams for decoding \cite{Schwarz2007}. It means, it is not necessary to require all sub-streams at the receiver to decode the video as done in older techniques (e.g. H.264). Typically, sub-stream at the base is sent first to achieve the low-resolution and low-frame-rate video, extra sub-streams are sent later to enhance the perceived quality. 

In modern video application (e.g. Dynamic Adaptive Streaming over HTTP or DASH) senders can select the appropriate sub-streams to send in order to adapt the throughput demand to the available throughput. The ability to adapt the scalable video streams to system conditions is referred as \textbf{video adaptation}. Consequently, video delivery, which supports video adaptation, is referred as Adaptive Video Streaming.

The video transmission rate measured per GOP is then defined as the ratio of total data amount of transmitted sub-streams to the duration of GOP. In general, the video transmission rate is smaller than the video coding rate of the video encoder. For the sake of readability, we however assume the video coding rate is equal the transmission rate.

\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{../../Figures/Illustrations/exampleSVCbitstream.png}
\caption{Illustration of the layered structure of coded video packages}
\label{fig:GOP}
\end{figure}

One can realize the video adaptation based on three scalability types, which are: temporal, spatial and quality scalability \cite{Schwarz2007}. While the temporal scalability allows system to change the image frame rate, the spatial scalability means the change of the spatial image resolution. With quality scalability, system can change the fidelity, which is commonly represented by quantization parameters. Figure \ref{fig:GOP} illustrates a join application of three scalabilities. In this example, there are two spatial layers (QCIF and CIF), five levels of frame rate (marked by number from 0 to 4), and two levels of fidelity (Coarse Grain and Medium Grain Quality). In general, video adaptation can be seen as a discrete function of utility to video rate, and illustrated as the dash line in Figure \ref{fig:generalUtilityFunction}. 

\begin{figure}[h]
\centering
\includegraphics[width=0.3\textwidth]{../../Figures/Illustrations/generalUtilityFunction.png}
\caption{Illustration of the layered structure of coded video packages}
\label{fig:generalUtilityFunction}
\end{figure}

Roughly speaking, the combinatory set of the possible scalability usage can be very large. Therefore it is reasonable to assume a very fine granular video adaptation. Then we generalize the video adaptation as an increasing continuous function (shown as the solid line in Figure \ref{fig:generalUtilityFunction}) as several works in literature. Additionally, for a realistic model, let $R_{min}$ and $R_{max}$ be the minimum and maximum video effective coding rate for GOP required for successful decoding, respectively. It means (1) any transmission rate of GOP under $R_{min}$ give no images and thus no utility, and, on the other hand, (2) the utility cannot be improved when transmission rate exceeds $R_{max}$. Then, when the transmission rate is greater than $R_{min}$ and smaller than $R_{max}$, users achieve incremental quality gains by sending an arbitrary small amount of video data.

%We then define the effective video coding rate as rate of total amount of data user sends for one GOP to $T^{GOP}$. Furthermore, we refer the term \textbf{video adaptation} in this work to the ability to adapt the effective coding rate of the video stream to the available throughput.

%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%
\subsection{Video Quality Assessment}
\label{sec:QID}
%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%

The desired networks are essentially designed to meet the satisfaction of users, hence any evaluative criteria used for video delivery must match user's Quality of Experience (QoE) of user video streams. Unfortunately, QoE depends on several factors associated with the human psychological and biological systems. As a matter of fact, modeling QoE and important factors determining QoE are still open issues. 

The first possible method to evaluate QoE is based on the \textbf{subjective assessment}, in which some users give there subjective opinions after watching videos. Although this method can give a precise result, it is subjective, static, costly and not reproducible. This problem give rise to the need for \textbf{objective assessments}, and two most used model are Peak Signal to Noise Ratio (PSNR) and Structural Similarity (SSIM) (see \cite{KSeshadrinathan2010} for a thorough survey about video quality evaluation). Particularly, SSIM index correlates better with QoE than PSNR, but PSNR is much easier to apply, especially for highly complex systems such as a cross-layer one. Indeed, PSNR is the most common objective model for video quality.

%%%%%%%%%%%%%%%%%%
\subsubsection{Short-term quality}
%%%%%%%%%%%%%%%%%%

In this work, we base our video quality model on the PSNR model, where the quality is measured per GOP and is a function of coding rate: $Q_{m}^{l_m} = G(B_m^{l_m})$. We refer to the quality assessment based on PSNR as \textbf{short-term quality}, since it measures the quality in an individual GOP only. The calculation of PSNR $Q_{m}^{l_m}$ for user $m$ in GOP $l_m$ takes the form as follows \cite{KSeshadrinathan2010}
\begin{equation}
Q_{m}^{l_m}=10log_{10}(\frac{255^{2}}{MSE}) \qquad,
\label{eq:PSNR}
\end{equation}
where Mean Squared Error $MSE$ essentially represents the distortion between the original and the decoded video sequences. Consequently, the distortion of the reconstructed video at the receiver is assumably caused only by the video source compression, the transmission errors is deal with at lower layers in system (e.g. Hybrid Auto Repeat Request HARQ). 

Consequently, $MSE$ can be efficiently approximated based on the well-known video rate distortion model introduced in \cite{Stuhlmuller2000}.
\begin{equation}
MSE \equiv D_{e}(R_{e}) = \frac{\theta}{R_{e}-R_{0}}+D_{0} \quad ,
\label{eq:DistortionRate}
\end{equation}

where the rate distortion $D_{e}(R_e)$ measured per GOP is a function of average video coding rate $R_{e}$. Characteristics of user's video such are modeled by three constants denoted by $\theta$, $R_{0}$ and $D_{0}$. Values of these 3 constants change from GOP to GOP. In general, a video with high motion requires higher coding rate than the one with low motion for the same quality (i.e. PSNR). 

Figure \ref{fig:DistortionRate} illustrates the characteristics of video stream through the dependence of PSNR on video coding rate $R_e$. In this example, in comparison with the GOP sent by the first user, the one sent by the second user contains less motion, thus requires lower coding rate for a quality similar. This variety of video characteristics is referred as video diversities.

\begin{figure}[h]
\centering
\includegraphics[width=0.3\textwidth]{../../Figures/Illustrations/throughputFairness.eps}
\caption{Illustration of the variety of video charactersitics}
\label{fig:DistortionRate}
\end{figure}

%%%%%%%%%%%%%%%%%%
\subsubsection{Long-term quality}
%%%%%%%%%%%%%%%%%%

Using mere short term quality is insufficient to measure user's QoE. It is shown that the temporal variability of short-term quality (i.e. PSNR) between successive GOPs can significantly deteriorate user's perceived quality \cite{VJoseph2014}. Especially, the PSNR fluctuation can result in a QoE that is worse than the one of a more constant quality video with lower average PSNR value \cite{Yim2011}. Therefore, we take into account not only short-term quality but also the long-term quality in order to determine QoE. The long-term quality is supposed to represent the consistency of PSNR values between GOPs. In this work, we adopt the the QoE index in \cite{VJoseph2014} to measure the QoE, which is computed as follows:

\begin{equation}
QID = \sum_{m=1}^{M} \big( \mu_m - var_m \big) \quad ,
\label{eq:QID}
\end{equation}

where $\mu_m$ and $var_m$ are the mean and the variance of PSNR values over all GOPs of video sequence, respectively. We have
\begin{equation}
\mu_m = \frac{1}{L} \sum_{l=1}^L Q_m^l \qquad var_m = \frac{1}{L}\sum_{l=1}^L \big( Q_m^l - \mu_m\big)^2
\end{equation}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{General Design of the Cross Layer System}
\label{sec:System}
%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%

In this section, we describe the general design for the cross layer system, which combines the video system in the Application layer and the OFDMA transmission system in lower layers. The general design of the system under investigation is illustrated in the Figure \ref{fig:GeneralSystem}.

\begin{figure}[h]
\centering
\includegraphics[width=0.4\textwidth]{../../Figures/Illustrations/GeneralSystem.eps}
\caption{System combining video adaptation and OFDMA resource adaptation}
\label{fig:GeneralSystem}
\end{figure}

In general, the main features of the system are described as follows:

\begin{itemize}
\item First, video sources are coded into scalable video streams by using standards such as HEVC. Additionally, the characteristics of all coded GOPs (i.e. three constants $\theta$, $R_0$ and $D_0$ for each GOP) are assumably available for the video adaptation purpose. Besides, coded video streams are organized into small packages, each of which contains some number of bytes as described in the SVC extension of H.264/AVC \cite{Schwarz2007}.

\item When a GOP is scheduled to be sent, its video packages are then fed to the transmission queue of the OFDMA system. Especially, video packages are prioritized based on the significance of sub-streams, particularly, from the one on the base to the higher ones. It means MAC then attempts to send packages of the sub-streams on the base first then those of next higher sub-streams. The prioritization is necessary to assure the decoder can achieve the lowest perceived quality first, then incrementally increase the quality as it receives more packages later.

\item Based on the instantaneous system conditions, BS defines the resource allocation scheme, which can be either static (e.g. Blockwise) or adaptive. Based on resource allocation scheme, users take resource and stream their videos. In this work, video packages can assumably be segmented arbitrarily small and fed to MAC frames.

\item Depending on the system conditions, video packages corresponding to higher sub-streams might be discarded after the maximal tolerable latency $T^{lat}$. In general, the more packages are sent and received, the better quality user perceives at the receiver.

%\item Value of $T^{lat}$ should balance the following trade-off: (1) big $T^{lat}$ helps system avoid the outage as users 

%\item It is important to point out that, the video adaptation is represented through the ability to discard video packages (from transmission queue) and match the data rate of video streams to the available transmission rate.

\item For the simplicity, we assume all user's GOPs have an equal and static duration $T^{GOP}$, and transmission of GOPs by users in uplink are synchronous. Later in Chapter \ref{sec:CrossLayerDesign}, we further discuss about the case of asynchronous GOPs and heterogeneous size. Particularly, we claim that the proposed mathematical formulation is still applicable, but an extension for the engineering perspective is necessary.

\end{itemize}

It is important to mention that, we do not explicitly consider the type of video delivery and its requirement for latency. (Two examples of video delivery types are (1) the interactive video conference (e.g. Skype) with stringent  latency requirement, and (2) Video On Demand (VOD) streaming (e.g. Netflix and Youtube) that robuster against latency.) We generalize different types of video delivery by a maximal tolerable latency value $T^{lat}$ of transmitting video GOPs. Particularly, scheduled GOPs are supposed to be sent during a duration of at most $T^{lat}$ from the moment they are put in the transmission queue of the OFDMA system; parts of GOP, which cannot be delivered during the duration of $T^{lat}$, are discarded. Obviously, the values of $T^{lat}$ of interactive applications are supposed to be much smaller than those of VOD streaming. For simplicity, we choose $T^{lat}=T_m^{GOP}, \forall m$.

%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%
\section{Time Scale Problem}
\label{sec:TimeScaleProblem}
%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%

The primary challenge in combining video adaptation of adaptive video streaming (conducted in the Application layer) and resource adaptation of dynamic resource allocation in OFDMA lies in the time scale difference described as follows:

\begin{itemize}
\item \textbf{Long term video adaptation:} Since the video coding and decoding work on the basis of GOP, the decision of video adaptation (at APP) takes place at the interval of GOP duration $T_m^{GOP}$, whose typical value is few hundred milliseconds long (e.g. 500ms).
\item \textbf{Short term resource adaptation:} On the other hand, in order to exploit the frequency and multiuser diversities and thus improve the spectral efficiency, the decision of Resource Allocation (at MAC) needs to occur at the pace equal the coherence time, which holds for few milliseconds in wireless networks (e.g. 5ms).
\end{itemize}

\begin{figure}[h]
\centering
\includegraphics[width=0.8\textwidth]{../../Figures/Illustrations/TimeScaleProblem_Full2.eps}
\caption{Illustration of the time scale difference}
\label{fig:TimeScaleProblem}
\end{figure}

Figure \ref{fig:TimeScaleProblem} illustrates the time scale difference of video adaptation and resource allocation. In general, one GOP is transmitted in a large number $K_m$ OFDMA frames (e.g. 100). During each OFDMA frame $k$ in GOP $l_m$, user $m$ takes some resources and sends some data bytes  $b_m^{k,l_m}$ $(0<k<K_m)$. Total amount of data $B_m^{l_m}$ user $m$ sends in the GOP $l_m$ is accumulated in all uplink frame $k$ in GOP $l_m$ and takes the following form

\begin{equation}
B_m^{l_m} = \sum_{k=0}^{K_m-1} b_m^{k,l_m}
\end{equation}

Then we have (as described in Chapter \ref{chap:SystemModel})

\begin{equation}
Q_m^{l_m} = G(B_m^{l_m}) = G(\sum_{k=0}^{K_m-1} b_m^{k,l_m})
\end{equation}

It means, quality values $Q_m^{l_m}$ need to be pursued through a sequential process of transmission of $K_m$ uplink frames. Unfortunately, since the time varying channel cannot be efficiently predicted for a long time span of GOP's duration, it is practically infeasible to compute the total amount of data $B_m^{l_m}$ sent for GOP $l_m$ before actually sending GOP $l_m$. Therefore, it is not reasonable to search for the optimal decision of video adaptation at the beginning of GOP as commonly implemented in the literature.

On the other hand, optimizing resource allocation without consideration of user's QoE most likely leads to a discrepancy between the supply and the user demand of throughput.

Therefore, a good cross-layer algorithm, which jointly optimize both video adaptation and resource adaptation in order to improve user's QoE, is supposed to work at two levels at the same time. In other words, the algorithm needs to couple the problem of resource allocation (occurring in each small span of coherence time) to the one of the video adaptation (occurring in large duration).
