%!TEX root = techreport.tex

\chapter{Performance Evaluation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Methodolody}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

We use the Monte Carlo method to evaluate the performance of the proposed cross-layer algorithm. Particularly, we develop a simulation based on the network simulation framework OMNeT++ and using the optimization solver Gurobi. Apart from the proposed algorithm based on \textbf{OPT-RA}, we simulate three other solutions as the baseline. We describe the main features of the simulated solutions as follows
\begin{itemize}

\item{Static Resource Allocation + Rate Adapting:}

In this solution, the resource allocation and video adaptation run independently. In every uplink frame, the assignment of subchannels to users conforms the simple block-wise strategy, where each user takes an equal and continuous block from the available bandwidth. The video transmission rate is equal the achieved throughput (thus named Rate Adapting). 

\item {Adaptive Resource Allocation + Rate Adapting:}

Similar to the first solution, resource allocation and video adaptation do not collaborate. The main difference is, Instead of static resource allocation, this solution deploys the dynamic resource allocation in \cite{Le2014}. Particularly, in this approach, for each uplink frame, resource allocation is optimized to maximize the minimum user throughput without considering how much user really benefits from the given throughput. Hence, this approach is referred as throughput-driven resource allocation. The video data transmission rate is then matched to the available throughput achieved at the solution of the max-min user throughput problem.

Essentially, the resource adaptation is also based on max-min fairness similar to \textbf{OPT-RA}. By comparing this solution, we investigate the benefit 

\item {Proportional-Fairness Resource Allocation + Rate Adapting:}

For a comparison with other works in literature, the solution in \cite{VJoseph2014} might be the most appropriate, as it also jointly consider the video adaptation and resource allocation in order to improve QoE for multiple video streams. However, since the main focus of that work actually deeply associates with several complex aspects of video delivery and the resource allocation is rather generous, we cannot efficiently simulate an equivalent model in this work. Therefore, we choose to compare with the baseline used in that work. Particularly, that system uses the proportional fairness approach for resource allocation and rate adapting as described above.

The optimization based on the proportional fairness approach is described as follows
\begin{equation}
max \sum \frac{1}{\rho_m} x_{i,m} F(\gamma_{i,m},P_{err})
\end{equation}
where $\rho_m$ is the peak throughput achieved when user $m$ takes all resources. The video data transmission rate is independently matched to the available resource.

\item{Quality-Driven RA:} Our novel cross-layer algorithm, which jointly combines the video adaptation and RA adaptation, as introduced in Chapter 3. 

\end{itemize}

For each resource allocation step, the same profile of the system condition is fed to all simulated solutions. 

Particularly, we simulate the simultaneous transmissions of $M$ streams by $M$ users. Users' video sequences have equal duration (i.e. same number of GOPs), and the size of all GOPs is static and equal for all users and all GOPs. Besides, GOPs are streamed synchronously in time. Characteristic of each GOP is defined by adopting the distortion rate parameters available in the literature. Then each video stream can be simulated as one sequence of tuples $(\theta,R_0,D_0)$, each of which is corresponding to one GOP.

Before each resource allocation step, all solutions consider the same system conditions, which are characteristics of GOPs and the channel conditions, and provide the resource allocation for the coming uplink frame. Based on the resource allocation, the simulation computes the SINR of subchannels, selects the appropriate ACM schemes, computes the total amount of data users send (i.e. $b_m^{k_m,l_m}$).

After the final uplink frame of the GOP that has been sent, the video quality value (i.e. PSNR) of GOP is computed based on the video data transmission rate of that GOP, i.e. $Q_m^{l_m}=G(\sum_{k_m} b_m^{k_m,l_m})$.

We collect all numerical results and evaluate the performance of different systems.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Simulation Setup}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

We assume there are 4 users are streaming videos in uplink. Each video streams consists of 30 GOPs, and each GOP in its turn includes 25 OFDMA uplink frames, i.e. $K_m=25, \forall m$. To simulate the GOP's characteristics, we adopt the distortion rate curves introduced in \cite{DWang2011}. In total, we use 4 different video characteristic profiles to simulate user's GOPs. We illustrate these data profiles in Figure \ref{fig:RDCurves}.

\begin{figure}[h]
\centering
\includegraphics[width=0.43\textwidth]{../../Figures/Illustrations/DR.eps}
\caption{Video Rate Distortion Curves}
\label{fig:RDCurves}
\end{figure}

Furthermore, to simulate the change of characteristics between GOPs in user's video sequence, we adopt the following policy. At the beginning, 4 GOPs sent by 4 users have the characteristics following 4 different data profiles, particularly user $m$ takes profile $m$. After an interval of 5 GOPs,  each user takes the profile with the index increased by 1, i.e. user $m$ now takes profile $m+1$ and, especially user 4 now takes the profile 1. The process continues similarly until the end of video sequences.

The main parameters for the OFDMA system are listed in Table \ref{tab:simPar}.
\begin{table}[h]
\caption{Simulation parameters for channel.}
\centering
\begin{tabular}{|l|l|}
\hline
Parameters & Values \\ 
\hline
% System
Number of users, i.e. no. video streams & $4$ \\
Number of subcarriers & $512$ \\
Number of subchannels & $8$ \\
Subcarrier spacing & $10940Hz$ \\
Number of subcarriers per subchannel & $64$ \\
Power per subcarrier & $1mW$ \\
\hline
MS maximal time offset & $2 \mu s$ \\
MS maximal frequency offset & $200Hz$ \\
\hline
% Channel
Cell radius & $100 m$ \\
Mobility model & Manhattan Mobility Grid \\
Path loss model & COST231 Walfish-Ikegami \\
Log normal shadowing std. dev. & $10 dB$ \\
Multipath fading model & Jakes's/Clarke's model \\
Thermal noise density & $-174 dBm/Hz$ \\
Penetration and other losses & $10 dB$ \\
%Receive Antenna Gain & $14 dB$ \\
%Delay spread (rms) & $0.251 \mu s$ \\
\hline

%\hline
%$v_{2}$ & equal the maximal delay spread \\
%$v_{1}$ &  0 $\mu s$ \\
\hline
\end{tabular}
\label{tab:simPar}
\end{table}

Total 30 runs are conducted for each solution's simulation. For each run, at the beginning, users' locations are defined in the way that their distances to BS in cell are uniformly distributed. During the process of video streaming, users' mobility is modeled based on the Manhattan Mobility Grid. In each run, 750 optimization instances (for 30 GOPs times 25 uplink frames) times 4 solutions are formulated and solved.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Numerical Result}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

First, we show results achieved from one of 30 runs in order to show the typical trend of throughput and quality. All results for 30 runs are shown in the Appendix.

%Run 1

%~~~~~~~~~~~~~~~~~~~~~~~~~~~
%Method 1: Blockwise + Rate adapting
%~~~~~~~~~~~~~~~~~~~~~~~~~~~

\begin{figure}[h]
\centering
\subfloat{}
	\includegraphics[width=0.28\textwidth]{../../Figures/Results/eps/R0_01.eps}
\subfloat{}
	\includegraphics[width=0.28\textwidth]{../../Figures/Results/eps/Q0_01.eps}
\caption{Blockwise + Rate adapting}
\end{figure}

%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%Method 2: Max-min throughput + Rate adapting
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

\begin{figure}[!h]
\centering
\subfloat{}
	\includegraphics[width=0.28\textwidth]{../../Figures/Results/eps/R1_01.eps}
\subfloat{}
	\includegraphics[width=0.28\textwidth]{../../Figures/Results/eps/Q1_01.eps}
\caption{Max-min throughput + Rate adapting}
\end{figure}

%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%Method 3: Cross layer max-min weighted quality
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

\begin{figure}[!h]
\centering
\subfloat{}
	\includegraphics[width=0.28\textwidth]{../../Figures/Results/eps/R7_01.eps}
\subfloat{}
	\includegraphics[width=0.28\textwidth]{../../Figures/Results/eps/Q7_01.eps}
\caption{Proportional Fairness + Rate adapting}
\end{figure}

%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%Method 4: Proportional Fairness + Rate adapting
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

\begin{figure}[!h]
\centering
\subfloat{}
	\includegraphics[width=0.28\textwidth]{../../Figures/Results/eps/R6_01.eps}
\subfloat{}
	\includegraphics[width=0.28\textwidth]{../../Figures/Results/eps/Q6_01.eps}
\caption{Cross layer max-min weighted quality}
\end{figure}
 
%\newpage

From the simulation results, we observe the trends of throughput and PSNR of each solution (in separation) as follows. 
\begin{itemize}

\item Static (Blockwise) Resource Allocation + Rate Adapting (BW+RA): Since the resource allocation based on Blockwise scheme is simple and static, users' accumulated throughputs strongly depend on the topology and thus significantly fluctuate between GOPs. Therefore, as the video transmission rate then matches the accumulated throughput, PSNR values vary accordingly, especially, as user's characteristic changes after every five GOPs.

\item Adaptive Resource Allocation (based on Max-Min user throughput) + Rate Adapting (MMT+RA): Since this solution aims at a throughput fairness, the users' throughputs are similar between GOPs as expected. However, the throughput fairness cannot lead to a fairness in PNSR, since users' videos require different throughput for a similar quality. As we can see, optimizing the resource allocation without consideration of how much users can benefit from the assigned resource can lead to the significant jump in PNSR. The change of PSNR can be easily seen when the GOP characteristics change.

\item Proportional Fairness + Rate Adapting (PF+RA): Using proportional fairness, users achieve throughput proportional to their peak rate, which are ultimately defined by the channel condition. In general, the approach based on proportional fairness can improve the overall efficiency (i.e. the sum of all throughput in cell) but lead to the dramatic variability of both throughput and thus quality. 

\item Cross-layer algorithm (CLA): By allocating resource with subject not to throughput, but quality (i.e. quality-driven resource allocation), this novel approach can provide stable quality fairness between GOPs as well as between users.

\end{itemize}

Most importantly, we compute the QoE index to measure the user's QoE. Figure \ref{fig:qid} show the average of QoE over all 120 simulated video streams (i.e. 4 video streams per run times 30 runs). As it shows, the novel cross-layer algorithm significantly improves the QoE, particularly by about 75\% and 40\% in comparison to those of the \textbf{(MMT+RA)} and \textbf{(PF+RA)}.

\begin{figure}[h]
\centering
\includegraphics[width=0.5\textwidth]{../../Figures/Results/Analysis/qoe.eps}
\caption{QoE Index (QID)}
\label{fig:qid}
\end{figure}

%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Next, we analyze in details the throughput and PSNR values achieved to find out what is the cause for the QoE improvement. To demonstrate all results, we show in Figure \ref{fig:cdfRQ} the cumulative distribution function (CDF) of all 3600 pair values of throughput and PSNR collected for 3600 GOPs that simulated (30 runs times 4 users times 30 GOPs) of each solution. Through the CDF, we analyze the variability of throughput and quality as well as the dependence of the solution's performance to the topology. Besides, we show in Figure \ref{fig:meanRQ} the average with standard deviation of the all throughput and PSNR values achieved in different solutions.

\begin{figure}[h]
\centering
\subfloat{}
	\includegraphics[width=0.4\textwidth]{../../Figures/Results/Analysis/cdfR.eps}
\subfloat{}
	\includegraphics[width=0.4\textwidth]{../../Figures/Results/Analysis/cdfQ.eps}
\caption{CDF of throughput and PSNR values}
\label{fig:cdfRQ}
\end{figure}

\begin{figure}[!h]
\centering
\subfloat{}
	\includegraphics[width=0.4\textwidth]{../../Figures/Results/Analysis/meanR_withErr.eps}
\subfloat{}
	\includegraphics[width=0.4\textwidth]{../../Figures/Results/Analysis/meanQ_withErr.eps}
\caption{Mean of throughput and quality}
\label{fig:meanRQ}
\end{figure}

With respect to the throughput, static resource allocation cannot use efficiently the available bandwidth. Meanwhile, as expected, the max-min user throughput can improve the avg. throughput with very low variability; most independent from the topology (shown as the steepest line in left side of Figure \ref{fig:cdfRQ}), the avg. throughput is about $82kbps$. Interestingly, the cross layer algorithm offers the biggest throughput variability. This can be explained by the different demand of throughput for user's GOPs. Finally, the resource allocation based on proportional fairness can provide the highest avg. throughput with second biggest throughput variability, which is caused directly by the channel condition.

On the right side of the two figures, we show the similar results for PSNR. As it can be easily seen, the cross-layer algorithm can significantly reduce the PSNR variability, but provide lower PSNR mean. Actually, the proportional fairness and max-min user throughput, which run independently from the video adaptation, can provide better avg. PSNR. This can be explained by the nature of the max-min fairness, where the worse user essentially limits the whole system performance.

Therefore, the main cause for the QoE improvement is the efficient reduction of the PSNR fluctuation between GOPs. In the other words, the novel cross layer algorithm can match the resource allocation efficiently to the actual demand of users, as the user's streams have different characteristics from GOP to GOP. More interestingly, as shown in the Appendix, our novel cross layer algorithm can perform independently from the topology. The PSNR values for all users in all 30 runs vary around $25dB$.

%the first solution with static resource allocation cannot use efficiently the available bandwidth, the achieved throughput is the worse case. Its corresponding quality is qualityMax-min user throughput clearly can reduce the variability of throughput (seen as the steepest line in the left figure)   the cross-layer algorithm can provide the steepest curve, which mean a very stable PSNR values independently from the topology (topology changes in each run).

%between GOPs, strong fluctuation in throughput and quality can be easily seen. On the other hand, as we will show later, this approach though cannot provide either throughput or quality fairness, it can indeed increase the total throughput in cell compared to the second best max-min fairness, which is essentially limited by the worse user (by the nature of max-min fairness).



%\begin{figure}[h]
%\centering
%\subfloat{}
%	\includegraphics[width=0.4\textwidth]{../../Figures/Results/Analysis/meanQ.eps}
%\subfloat{}
%	\includegraphics[width=0.4\textwidth]{../../Figures/Results/Analysis/stdQ.eps}
%\caption{Mean and Standard Deviation of PSNR values}
%\end{figure}

