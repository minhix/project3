%!TEX root = techreport.tex

\chapter{Novel Cross Layer Design} \label{chap:CrossLayerDesign}

In this work, we develop a novel cross-layer system combining adaptive video streaming  (in the Application layer) and the dynamic resource allocation (in OFDMA system) in order to improve user's perceived quality (i.e. QoE) of multiple streams in uplink. To improve QoE, a novel cross-layer algorithm exploits the video adaptation and resource adaptation together. And the algorithm pursues the QoE improvement in the sequential manner based on a series of optimizations of quality-driven resource allocation in order to deal with the time scale problem. The algorithm is supposed to improve the system performance beyond the individual gains by two adaptations in separation.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Model for improving QoE} \label{sec:improvingQoE}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In this work, we address three main aspects determining user's QoE as described in the following. Note that the discussion in this section essentially associate only to the application layer. 

\begin{itemize}
\item Short-term Quality: Obviously the higher PSNR of GOP, the better short-term quality in that GOP. Therefore, if PSNR of all GOPs increase, user's QoE is supposed to increase too.
\item Apart from short term quality, we would like to reduce the PSNR fluctuation between GOPs in order to improve QoE.
\item Finally, since all users should be served equally, a quality fairness between uses should be included. 
\end{itemize}

To improve QoE, one might formulate the problem to optimize directly the QoE index (i.e. QID described in Section \ref{sec:QID}). Unfortunately, as discussed later in Section \ref{sec:otherQoEStrategy}, an optimization problem maximizing QID then leads to a max-sum problem, which tends to increase the efficiency but not a quality fairness between users. Moreover, due to the quadratical term of the variance (i.e. $\epsilon_m$) in the formulation of QID, that approach also gives rise to a severe mathematical challenge.

In general, through the integration of quality values of transmitted GOPs into the problem for currently being sent GOPs (realized here by the PSNR normalization), system can optimize the video adaptation in the inter-GOP manner rather than for individual GOP separately. The inter-GOP optimization might be useful to assure a quality consistency, as the characteristics of video sources changes from GOP to GOP and from user to user and time varying channel is unpredictable. For example, user $m$ is about to stream GOP $l_m$ with less motion than those already transmitted, thus GOP $l_m$ requires less resource for a similar quality as those transmitted. In that case, user $m$ should take less resource than before to maintain a similar or slightly better quality for GOP $l_m$ in order to (1) avoid a big change in quality and (2) spare other users some resource. Obviously, choosing weight equal PSNR average is not the only way for developing inter-GOP video adaptation, in this work we simply choose that for simplicity as we demonstrate the novel optimization concept.

Therefore, we heuristically formulate the optimization problem to maximize the minimum (over all users) weighted PSNR values. While the max-min optimization can encompass the quality fairness and the improvement of short term quality, the PSNR values are weighted by the average of achieved PSNR values (over all transmitted GOPs) to reduce the PSNR fluctuation. Such proposed weights can be also seen as the normalization of PSNR. We refer to this max-min weighted problem as \textbf{OPT-VID}. One instance of \textbf{OPT-VID} is formulated for one group of GOPs that being sent by users at the moment of consideration.

We then formulate the instance of OPT-VID for the set of GOPs, denoted by $l$, that users are simultaneously streaming at the moment of consideration, $l=(l_m, 1\leq m \leq M)$. The formulation is shown in the following.

\begin{equation}
\centering
\begin{split}
\textbf{OPT-VID:} \qquad & \\
&\max \quad \Phi \\
s.t. \quad &\frac{Q_m^{l_m}}{\mu_m} \geq \phi \qquad ,\forall m
\end{split}
\label{eq:simple_maxminQ}
\end{equation}
where $\mu_m$ is the average PSNR achieved over all transmitted GOPs.
\begin{equation}
\mu_m = \frac{1}{l_m} \sum_{i=1}^{l_m} Q_m^{l_m - i}
\label{eq:muQ}
\end{equation}

Note that quality values of transmitted GOPs (i.e. $Q_m^{l_m-i}$ in \eqref{eq:muQ}) are known at the moment of consideration, and thus $\mu_m$ is a constant in \eqref{eq:simple_maxminQ}.

It is important to note that, one can take into account a general value of maximal tolerable latency, i.e. $T^{lat}\geq1$. It means, data for one GOP will be discarded after a period of $T^{lat}$, equal $W$ GOPs. Consequently, at each moment during the streaming process, users can stream data for $W$ most recent GOPs. In general, a large $W$, on one hand, can reduce the outage of video delivery when user throughput is temporary low (e.g. shadowing loss caused by a big building), but, on the other hand, increases the latency of video decoding. Value of $W$ of a practical scenario should balance that trade-off of latency vs. efficiency. In this work, for the sake of simplicity, we restrict our formulation to the case $W=1$, and skip the index for user's currently-being-sent GOP. The selection of $W=1$ can fit well for the video streaming, which requires a low-latency (e.g. the maximal one-way tolerable latency for video conference is from 150 to 200 ms).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Novel Cross Layer Algorithm}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In general, from the perspective of video application, solving \textbf{OPT-VID} might provide proper decisions for video adaptation to improve QoE. However, due to the time sale problem, the desired QoE improvement needs to be pursued \textit{"on-the-fly"} during the process of sending a large number of uplink frames of GOPs.

In this section, we develop and show the novel algorithm, which works in the sequential manner based on a series of quality-driven resource allocation. 

It is important to to mention that, here, we formulate the problem with three assumptions: (1) the GOP duration are the same for all users, (2) GOPs are sent synchronously, and (3) each user's video streams contains a same number of GOPs. An extra discussion for the general case with heterogeneous, asynchronous GOPs and different stream lengths are given later in this Chapter. Particularly, we claim that the proposed mathematical formulation can be still applicable, but the case requires some engineering extends.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The sequential process of quality-driven resource allocation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

We illustrate the relation between the large-scale video adaptation and the small-scale resource adaptation as well as the sequential approach in Figure \ref{fig:OPLevels}. As it shows, we pursue the goal for video adaptation for GOP $l_m$ of user $m$ through a series of quality-driven resource allocation, each of which takes place for one uplink frame.

\begin{figure}[h]
\centering
\includegraphics[width=0.8\textwidth]{../../Figures/Illustrations/OP_Levels.eps}
\caption{The sequential process deals with the time scale problem}
\label{fig:OPLevels}
\end{figure}

Let us assume that, at the moment, user $m$ is sending the uplink frame $k_m^*$ in the middle of GOP $l_m$. At this moment, only channel condition for the coming uplink frame $k_m^*$ is known. Before the current uplink frame, user $m$ has sent an accumulated data amount of $\tilde{b}_m^{k^*_m-1,l_m}$. The algorithm currently needs to define the resource allocation, based on which users will takes resources, send a data amount of $b_m^{k^*_m,l_m}$. Then, we write
\begin{equation}
\tilde{b}_m^{k^*_m,l_m} = \tilde{b}_m^{k^*_m-1,l_m} + b_m^{k^*_m,l_m} \quad ,
\end{equation}

\begin{figure}[h]
\centering
\includegraphics[width=0.45\textwidth]{../../Figures/Illustrations/exBitrateVsQuality.eps}
\caption{Illustration of the approach for the novel cross layer algorithm}
\label{fig:intermediateQuality}
\end{figure}

Clearly, we have $\tilde{b}_m^{k^*_m,l_m}=B_m^{l_m}$ when $k_m^*$ is the last uplink frame in the GOP (i.e. $k_m^* = K_m$). Corresponding to the accumulated amount of transmitted data $\tilde{b}_m^{k^*_m-1,l_m}$, we then estimate the intermediate quality $q_m^{k^*_m-1,l_m}$  as follows:

\begin{equation}
q_m^{k_m^*,l_m}= G(\tilde{b}_m^{k_m^*,l_m}) = q_m^{k_m^*-1,l_m} + g(b_m^{k^*_m,l_m})
\label{eq:g}
\end{equation}

In \eqref{eq:g}, function $g(.)$ computes the usefulness of the resource (i.e. $b_m^{k^*_m,l_m}$) with respect to the quality. Figure \ref{fig:intermediateQuality} demonstrates the gains of throughput and intermediate quality in the uplink frame $k_m^*$ inside the GOP $l_m$.

It is important to note that, since the channel condition for the rest of uplink frames of the current GOP $l_m$ is practically unknown, it might be the case that user $m$ cannot send any data after uplink frame $k_m^*$. Hence, the intermediate quality $q_m^{k_m^*,l_m}$ becomes the overall quality $Q_m^{l_m}$. Therefore, the algorithm is supposed to treat any uplink frames in middle of GOP as the last one. It means, the achievements of resource adaptation in each uplink frame should be asymptotic to the video adaptation goal.

For that reason, we pursue the optimization problem of \textbf{OPT-VID} in a series of optimization problem of quality-driven resource allocation, referred as \textbf{OPT-RA}. At the beginning of every uplink frame, one instance of OPT-RA is formulated and solved to find the appropriate resource allocation scheme, which can improve the intermediate quality toward the desired quality value. The general formulation of OPT-RA is described as follows

\begin{equation}
\centering
\begin{split}
\textbf{OPT-RA:} \qquad & \\
&\max \quad \Phi \\
s.t. \quad &\frac{q_m^{l_m}}{\mu_m} \geq \phi \qquad ,\forall m
\end{split}
\label{eq:simple_maxminq}
\end{equation}

Note that, we skip the index for uplink frame $k_m^*$ in \eqref{eq:simple_maxminq} for the sake of readability. It is then clearly that the coupling function $g(.)$ is the key to link the transform the video adaptation goal to the (series of) resource adaptation goal.

In next sections, we investigate the details of the coupling function and the dynamic resource allocation, which are essential to derive the final formulation of OPT-RA. 

%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%
\subsection{Coupling function}
%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%

Function $g(.)$ is essentially required to estimate the intermediate quality achieved in middle uplink frames of the currently-being-sent GOP. Based on this coupling function, the algorithm drives the resource adaptation of the every uplink frame, so that the intermediate quality is incrementally improved toward the desired  quality $Q_m^{l_m}$ by the end of the GOP.

In this section, we show the simple but yet sufficient formulation of the coupling function $g(.)$. First, we have

\begin{equation}
\begin{split}
Q_m^l &=10log_{10}(\frac{255^2}{D_e}) \\
&=10log_{10}(255^2) - 10log_{10}(\frac{\theta}{R_e - R_0} + D_0) \\
\end{split}
\label{eq:U}
\end{equation}
then we take the derivative of PSNR $Q_m^l$ to $R_e$
\begin{equation}
\frac{\delta Q_m^l}{\delta R_e} = \frac{10\theta}{ln(10)} \times \frac{1}{(\theta+D_0(R_e - R_0))(R_e - R_0)}
\label{eq:derivativePSNR}
\end{equation}

And since one GOP typically consists of a large number of uplink frames, the PSNR gain in each uplink frame is relatively small to the overall PSNR value for the GOP. Hence, it is reasonable to approximate 

\begin{equation}
\delta R_e \approx c_m^{k_m^*,l_m}=b_m^{k_m^*,l_m}/T_m^{GOP} \quad ,
\end{equation}
and
\begin{equation}
\delta Q_m^l \approx q_m^{k_m^*,l_m} - q_m^{k_m^*-1,l_m} = \Delta q_m^{k_m^*,l_m} = g(b_m^{k_m^*,l_m})
\end{equation}

Consequently, we have

\begin{equation}
g(b_m^{k_m^*,l_m}) = c_{m}^{k_m^*,l_m} \times \frac{10\theta}{ln(10)} \frac{1}{(\theta+D_0(\tilde{c}_{m}^{k_m^*-1,l_m} - R_0))(\tilde{c}_{m}^{k_m^*-1,l_m} - R_0)}
\label{eq:exact_g}
\end{equation}

where $\tilde{c}_m^{k_m^*-1,l_m}=\tilde{b}_m^{k_m^*-1,l_m}/T_m^{GOP}$. Note that, at the uplink frame $k_m^*$, variables $\tilde{c}_m^{k_m^*-1,l_m}$ are all defined. And three parameters $\theta$, $D_0$ and $R_0$ are assumably known as described before. Thus, for simplicity, we can write:

\begin{equation}
\tilde{q}_{m}^{k_m^*,l_m}=\tilde{q}_{m}^{k_m^*-1,l_m} + \alpha c_{m}^{t,l}
\label{eq:exact_g}
\end{equation}
 where the constant $\alpha$ takes the following form
\begin{equation}
\alpha = \frac{10\theta}{ln(10)} \frac{1}{(\theta+D_0(\tilde{c}_{m}^{k_m^*-1,l_m} - R_0))(\tilde{c}_{m}^{k_m^*-1,l_m} - R_0)} \quad .
\end{equation}

Function $g(.)$ becomes a very simple linear function of the intermediate throughput $c_{m}^{t,l}$. The simplicity of $g(.)$ definitely provides a huge computational benefits compared to one derived by using \eqref{eq:PSNR} and \eqref{eq:DistortionRate} (i.e. \eqref{eq:U}) as we design the cross layer algorithm, which jointly consider the video adaptation and the resource adaptation.

At the beginning of each uplink frame, BS compute the accumulated values $\tilde{c}_{m}^{k,l_m}$ based on the accumulated amount of data that been sent (i.e. $\tilde{b}_{m}^{k,l_m}$, and uses the knowledge of the video (i.e. $\theta$, $D_0$ and $R_0$) to formulate the actual formulation of function $g(.)$. Consequently, the cross-layer algorithm can use $g(.)$ to integrate the constraints of the video adaptation problem (in big-scale) into a series of problem of quality driven resource allocation.


%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Dynamic Resource Allocation}
%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%

We continue the work by considering the problem of resource allocation in order to develop the appropriate quality-driven resource allocation. As mentioned in Chapter 2, we adopt the approach introduced in our previous paper \cite{Le2014} in order to (1) efficiently exploit the multi-user and frequency diversities in  order to increase the spectral efficiency, and, at the same time, (2) dynamically adapt the resource allocation to requirements of user's QoE.

It is important to mention that, by adopting this approach of dynamic resource allocation, we can also take into account the practical problem of imperfect synchronization between users, and, thus, the significant degradation of Multiple Access Interference (MAI).

The key point of the resource allocation is the selection of the modulation and coding schemes, which is represented in this work through the function $F(.)$. This function takes the SINR values $\gamma_{i,m}$ of subchannel $i$ of user $m$ and selects the appropriate ACM scheme subject to the tolerable error rate $P_{err}$. Essentially, function $F(.)$ is non-linear, non-continuous and thus non-convex as illustrated in Figure \ref{fig:ACM}, where $Th_{s}$ and $R_{s}$ are the required SINR and the number of bits sent on one subchannel $i$, when the ACM scheme $s$ is chosen from total $S$ available ACM schemes, respectively.

\begin{figure}[ht]
\centering
\includegraphics[width=0.33\textwidth]{../../Figures/Illustrations/SINR_Bit.jpg}
\caption{Adaptive Coding and Modulation function $F(.)$}
\label{fig:ACM}
\end{figure}

Fortunately, by exploiting the piece-wise linear function \cite{Lee2012}, we introduce the much simpler and, yet, equivalent formulation of $F(.)$. First, we describe this function in Algorithm \ref{alg:conditionalF}.

\begin{algorithm}[ht]
\For{\texttt{$(1 \leq s \leq S)$}}{
	\If{$(Th_s < \gamma_{i,m})$}{
		$b_m = R_{s}$
	}
}
\caption{Algorithm for ACM selection} \label{alg:conditionalF}
\end{algorithm}

Furthermore, we can simplify ACM selection constraint as follows:
\begin{equation}
\begin{split}
Th_s  \leq \gamma_{i,m} &\Leftrightarrow Th_s \leq 10log_{10} ( \frac{P_{i,m}^{RX}}{\sigma^2 + MAI_{i,m}^{t}} ) 	\\
&\Leftrightarrow \widetilde{Th}_{i,m,s}  \geq \frac{MAI_{i,m}^{t}}{\sigma^2} \\
&\Leftrightarrow \widetilde{Th}_{i,m,s}  \geq \sum_{j\neq i} \sum_{m' \neq m} \frac{MAI_{i,m}^{j,m'}}{\sigma^2} \\
\end{split}
\label{eq:newF}
\end{equation}
where $\widetilde{Th}_{i,m,s} = 10^{(P_{i,m}^{RX}/ \sigma^2-Th_s)/10} - 1$.

Now we define binary variable $x_{i,u}$ to represent the resource allocation, $x_{i,u}$ takes 1 if user $u$ takes subchannel $i$ and 0 if not. Consequently, $x_{i,m} F(\bar{\gamma}_{i,m}, P_{err})$ is the number of data (in bits) that user $m$ can send on subchannel $i$. Then, we have
\begin{equation}
b_m^{k_m^*} = \sum_{i=0}^{N_{SCh}-1} x_{i,m} F(\gamma_{i,m}, P_{err})
\label{eq:basicF}
\end{equation}

To facilitate the transformation based on the piece-wise linear function, we introduce new auxiliary optimization variables for the necessary transformation: (1) $\Omega_{i,m}$ is the number of bits user $m$ sends on subchannel $i$, (2) $z_{i,m,s}$ takes "1" if scheme $s$ is used in subchannel $i$ of user $m$. Then we transform \eqref{eq:basicF} into the following forms

\begin{equation}
\centering
\begin{split}
& b_m^{k_m^*} =  \sum_{i=0}^{N_{sch}-1} x_{i,m} \Omega_{i,m}\\
& \Omega_{i,m} \leq \sum_{s=0}^{S} z_{i,m,s} R_{s} \\
& \sum_{s=0}^{S} z_{i,m,s} \widetilde{Th}_{i,m,s} \geq \big( \sum_{j} \sum_{m'} \frac{\overline{MAI}_{i,m}^{j,m'}}{\sigma^2} x_{j,m'} \big) \quad , \forall i,m \\
& \sum_{s=0}^{S} z_{i,m,s} = 1 
\end{split}
\label{eq:novelOP}
\end{equation}

Finally, the problem of resource allocation, represented through function $F(.)$, can be seen as an equivalent set of linear and continuous function.

%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Quality Driven Resource Allocation}
%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%

In this section, we combine the introduced coupling function $g(.)$ and the equivalent transformation of function $F(.)$ above to formulate the exact formulation of \textbf{OPT-RA} as shown in \eqref{eq:maxminq}.

\begin{equation}
\begin{split}
max & \quad \epsilon \\
s.t. \quad &a) \frac{1}{\mu_m} (q_{m}^{k_m^*-1} + \frac{\alpha_m^{l_m}}{T_m^{GOP}} \sum_{i=0}^{N_{sch}-1} x_{i,m} \Omega_{i,m}) \geq \epsilon, \qquad \forall m \\
&b) \Omega_{i,m} \leq \sum_{k=0}^{K} z_{i,m,s} R_{s} \quad, \forall i,m \\
&c) \sum_{k=0}^{K} z_{i,m,s} \widetilde{Th}_{i,m,k} \geq \big( \sum_{j} \sum_{u'} \frac{\overline{MAI}_{i,m}^{j,m'}}{\sigma^2} x_{j,m'} \big) \quad , \forall i,m \\
&d) \sum_{k=0}^{K} z_{i,m,s} = 1 \quad , \forall i,m \\
&e) \sum_m x_{i,m} \leq 1, \qquad \forall i ,
\end{split}
\label{eq:maxminq}
\end{equation}

where $\alpha_m^{l_m}$ is computed specifically for the current GOP $l_m$ of user $m$ based on the characteristics of the current GOP, i.e. $\theta_m^{l_m}$, $R0_m^{l_m}$ and $D0_m^{l_m}$.

It is important to note that, as described in Section \ref{sec:AVS}, the data transmission rate must exceed the minimum rate $R_{min}$ in order to decode a meaningful quality. Thus when $(\tilde{c}_m^{k_m^*-1,l_m} < R_{min})$, we have $\tilde{q}_m^{k_m^*-1,l_m}=0$ and $\alpha$ cannot reflect the usefulness of the resource anymore. In that case, we would heuristically transform the first constraint into the following form
\begin{equation}
\frac{1}{\mu_m} [ \frac{Q_{min}}{R_{min}} (\tilde{c}_m^{k_m^*-1} + \frac{1}{T_m^{GOP}}\sum_{i=0}^{N_{sch}-1} x_{i,m} \Omega_{i,m}) ] \geq \epsilon
\end{equation}

where $Q_{min}$ is the minimum quality corresponding to $R_{min}$. The above formulation is to assure, during the early time of GOP as the coefficient $\alpha$ is ineffective, all users achieve intermediate qualities proportionally to the minimum quality $Q_{min}$ corresponding to $R_{min}$. 

Most importantly, the problem in \eqref{eq:maxminQ}, which is essentially a general Mix-Interger Non-Linear Program (MINLP), is converted into an equivalent Mixed Integer Quadratically Constrained Problem (MIQCP) in \eqref{eq:maxminq}. In general, although MIQCP is still NP-hard, MIQCP has enough structure, which can be exploited in order to provide valuable computational benefits (see the work from Burer et. al in \cite{Lee2012}). Fortunately, efficient global techniques to solve MIQCP can be built base on strong convex relaxation and valid inequalities, therefore software optimization solvers such as Gurobi are able to solve instances of \eqref{eq:novelOP} and provide optimal Quality-Driven RA in order to improve QoE.

%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The novel cross-layer algorithm}
%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%

Therefore, we formulate the novel cross-layer algorithm as shown in Algorithm \ref{alg:NovelAlgorithm}. Essentially, we attempt to solve the problem \textbf{OPT-RA} at every single resource allocation step (that for one small channel coherence time span) in order to (1) increases the spectral efficiency and (2) improve the intermediate quality toward the wanted QoE improvement.

In general, the channel coherence time in cellular networks takes a duration of few milliseconds, thus equal a few uplink frames (e.g. 1 or 2). In this work, for the sake of readability, we assume the channel coherence time span the uplink frame are equally long. Then, one resource allocation process takes place in every uplink frames.

\begin{algorithm}[h]
\KwData{Knowledge of channel and user's video characteristics. Statistics about achieved short term quality in the past.}
\KwResult{Series of optimal Quality-Driven resource allocation improving QoE of video streams}
\For{$(l_m=0;l_m \leq N_m^{GOP}; l_m)$}{
	\For{$(m=1;m\leq M;m++)$}{
		Compute then mean  $\mu_m^{l_m}$ of short-term quality values achieved in previous GOPs \ignore{Compute statistics of short-term quality values achieved in previous GOPs, i.e.: mean $\mu_m^{l_m}$ and standard deviation $\epsilon_m^{l_m}$.}
	}
	\For{$(k=0; k \leq K; k++)$}{
		\For{$(m=1;m\leq M;m++)$}{
			\begin{itemize}
				\item Compute of the intermediate transmission rate $\tilde{c}_{m}^{k_m^*-1,l_m}$.
		
				\item Compare $\tilde{c}_{m}^{k_m^*-1,l_m}$ with $R_{min}$, then define $q_m^{k_m^*-1, l_m}$ and $\alpha$.
		
		\end{itemize}
		}
		 Formulate and solve the optimization \textbf{OPT-RA} to acquire the appropriate quality-driven resource allocation.
	}
}
\caption{Pseudocode illustrates the cross-layer algorithm improving the QoE of video streams}
\label{alg:NovelAlgorithm}
\end{algorithm}

The novel cross layer algorithm is supposed to pursue the big goal of QoE improvement through the sequential process based on a series of resource adaptation in order to efficiently deal with the time scale problem.

\begin{figure}[h]
\centering
\includegraphics[width=0.6\textwidth]{../../Figures/Illustrations/CrossLayer_SignalingFlow.eps}
\caption{General cross-layer design}
\label{fig:CrossLayerDesign}
\end{figure}

At the beginning of the resource allocation step, the algorithm formulate and solve the problem \textbf{OPT-RA}, which includes the channel conditions (represented by $\widetilde{Th}_{i,m,s}$), the time and frequency offsets (represented in $MAI$), the video characteristics and the intermediate achieved quality. By doing that, the algorithm drives the resource adaptation of the next uplink frame so that the intermediate quality is incrementally improved toward the desired overall quality by the end of the GOP. It means, at each step, given a specific system condition, the quality-driven resource allocation can actually exploit the diversities to use the most use out of the available bandwidth.

Furthermore, the algorithm can also exploit the difference of video sources, where users might send GOPs with different demand of resource in order to achieve a similar quality.

Finally, the corresponding system design for the novel algorithm is illustrated in Figure \ref{fig:CrossLayerDesign}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Discussion about asynchronous and heterogeneous GOPs}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In this section, we discuss further about the general case, where the GOP sizes are heterogeneous between users and users transmit their GOPs asynchronously. 

\begin{figure}[h]
\centering
\includegraphics[width=0.6\textwidth]{../../Figures/Illustrations/SYN_GOP.eps}
\caption{Illustration of the problem of video adaptation with asynchronous GOPs}
\label{fig:asynchGOP}
\end{figure}

First, from the perspective of mathematics, the proposed formulation can be still applicable for the asynchronous and heterogeneous (in size) GOPs. However, without any extension, users, who are sending the early part of GOP, might receive more resource than other. That unwanted fact stems from the difference of the accumulated data (i.e. $\tilde{b}_m^{k_m^*,l_m}$). Thus, one might extend the work to include time-varying weights, which reflects the remaining opportunities to achieve the wanted quality.

Furthermore, as illustrated in Figure \ref{fig:asynchGOP}, the approach of sequential process can help to alleviate the challenge of video adaptation as user's GOPs are asynchronous and heterogeneous in size.

On the other hand, one might assume that the central optimizer groups the streams, which have a similar video coding profiles, so that the coming GOPs have the same size. Besides, a small buffer (enough for one GOP) can help to synchronize the transmission of GOPs. Then such that mind assumption is practical.     

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Discussion about alternative approaches for improving QoE} \label{sec:otherQoEStrategy}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In this work, we attempt to improve user's QoE based on the optimization problem of max-min quality fairness. Alternatively, the authors in \cite{VJoseph2014} suggest to improve QoE by maximize the \textbf{QID} (which is introduced in \eqref{eq:QID}) as follows. The following equation is formulated for the set of currently-being-sent GOPs $l=(l_m,1\leq m \leq M)$.
\begin{equation}
\centering
\max_{Q} \sum_{m} \big(  \mu_m^{l_m} - var_m^{l_m} \big)
\label{eq:OPT-VID-2}
\end{equation}
where $Q=(Q_m^{l_m}, 1 \leq m \leq M)$, and $Q_m^{l_m}$ is the short term quality of user $m$ in GOP $l_m$. Particularly, the mean of quality $\mu_m$ and variance $var_m$ achieved after GOP $l_m$ can be formulated as below

\begin{equation}
\centering
\begin{split}
\mu_m^{l_m} &= \frac{l_m-1}{l_m}\mu_m^{l_m-1} + \frac{1}{l_m}Q_m^{l_m} \\
var_m^{l_m} &= \frac{1}{l_m} \sum_{l=1}^{l_m} \big( Q_m^{l} - \mu_m^{l_m} \big)^2
\end{split}
\end{equation}

In \cite{VJoseph2014}, the problem in \eqref{eq:OPT-VID-2} can be solved efficiently, as the work generalizes the resource allocation as a stationary ergodic process in order to cope with a variety of networks. Based on that model, the problem of resource allocation mainly focuses on how to distribute the throughput to users, rather than how to actually achieve the required throughput as we consider in this paper. In general, the work in \cite{VJoseph2014} is deeply associated with several complex challenges of video delivery (e.g. rebuffering rate and data cost), and the resource allocation is rather generous. That is also the reason why we could not directly compare our work with the one in \cite{VJoseph2014}.

However, to demonstrate the flexibility of our novel concept, where the problem of quality adaptation can be transformed into the sequential process of quality-driven resource adaptation, we transform the problem of the video adaptation in \eqref{eq:OPT-VID-2} to a series of  quality-driven resource allocation as below.

\begin{equation}
\centering
\max \quad \sum_{m} \big(  \frac{L-1}{L}\mu_m^{l_m -1} + \frac{1}{L}q_m^{l_m}- \frac{1}{L} \sum_{l=1}^{l_m-1} \big( Q_m^l - (\frac{l_m-1}{l_m}\mu_m^{l_m-1} + \frac{1}{l_m}Q_m^{l_m}) \big)^2 - \frac{1}{L}\big( q_m^{l_m} - \frac{l_m-1}{l_m}\mu_m^{l_m-1} + \frac{1}{l_m}Q_m^{l_m} \big)^2  \big)
\label{eq:maxsum}
\end{equation}

where $Q_m^{l}$ with $1\leq l < l_m$ are all known, and $q_m^l$ is formulated similarly as introduced in \eqref{eq:novelOP}. 

Conceptually, one can develop a cross-layer algorithm based on \eqref{eq:maxsum} to maximize directly QID as described in \eqref{eq:OPT-VID-2}, however such formulation leads to the mathematical challenge with the existence of the quadratical terms $(q_m^{l_m})^2$.

