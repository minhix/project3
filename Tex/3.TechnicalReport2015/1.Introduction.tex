%!TEX root = techreport.tex

\chapter{Introduction}
%\lipsum[3-33]

\section{Motivation}

Mobile video traffic is one of the main contributors to the exponential traffic growth in cellular networks. Particularly, it is currently accounting for approximately 55 percent of total mobile data traffic and expanding that share to about 72 percent by 2019 \cite{Cisco2014}. Absent comprehensive solutions, this trend could lead to a spectrum crunch as the resources are scarce and the frequency spectrum availability is limited. To address these challenges, various solutions have been proposed to meet the great demand, e.g. the densification Heterogeneous Network (HetNet), massive MIMO, directional antenna, relayer, etc. However, none of those solutions is particularly designed specifically for video delivery. Additional, even in such networks, the problems of dynamic interference and variability of available throughput (caused by, for example, time varying channels and dynamic number of active users associating to the common access point) raise a huge challenge for managing user's Quality of Experience (QoE) of received videos. Given these challenges, optimizing video delivery and, at the same time, maximizing the efficiency of spectrum usage are one of the primary networking problems today.

To facilitate more efficient video system, modern video coding standards, which are High Efficiency Video Coding (HEVC) \cite{Sullivan2012} and its predecessor H.264/Advance Video Coding (AVC) with Scalable Video Coding (SVC) extension \cite{Schwarz2007}, can encode the video input into scalable video stream. Based on that, advanced video delivery standards like Dynamic Adaptive Video over HTTP (DASH)) \cite{Sodagar2011} allow video clients adapt on the fly the video transmission rate of video stream to match constraints such as available bandwidth or requirements for desired quality. System of video delivery, which supports such video adaptation, is referred as adaptive video streaming. 

On the other hand, Orthogonal Frequency Division Multiple Access (OFDMA), which is used both in LTE-A and WiMAX, is favorable for its flexible and efficient spectral usage. Specifically, while the high spectral efficiency is offered by using orthogonal subcarriers, the flexible resource allocation can dynamically assign available resource to users, who can use them better according to the utility function (e.g. maximize the sum of utility, maximize the minimum utility). In general, dynamic resource allocation of OFDMA allows system to adapt the resource allocation scheme, which informs resource partitions users can take, to not only increase the spectral efficiency but also better serve user's need. 

Beyond the separate gains by video adaptation and adaptation of resource allocation in separation, tremendous demand for mobile video over cellular networks raises huge challenges not only concerning the efficiency in individual layers, but also efficient network designs. It is then intuitive to ask the following question: can a joint optimization of two adaptations help to increase user's video perceived quality beyond the sum of separate gains ? And if yes, how to do that efficiently ? The paradigm of cross-layer design, where non-adjacent layers can exchange information and collaborate, has attracted a large number of work in the last decade. Basically, as adaptations are conducted separately, it tends to give rise to a discrepancy between the resource supply by the OFDMA system and the actual throughput demand by the video stream. It means, one user might have resource that it does not really benefit from, while others can use spare resource better.

The focus of this work is the delivery of video streamed by users in the uplink of the multiuser OFDMA cellular networks. Great appeal for video streaming in uplink comes from applications like video conference and social networking. For instance, there might be thousands of simultaneous streams during big football matches or music concert \ignore{\cite{Cisco???}}. Particularly, we address the problem of jointly optimizing video adaptation together with the resource adaptation of OFDMA with the goal to improve user's QoE of received videos.

From the perspective of application layer, We manage to improve user's QoE in three main aspects: (1) increasing short term quality measured per one Group of Pictures (GOP), (2) reducing the variation of short term qualities between adjacent GOPs (i.e. increasing long-term quality), and (3) taking into account a quality fairness among users. Particularly, we base the video quality assessment on the model of Peak Signal to Noise Ratio (PSNR) measured per GOP. Then we cover all three aspects determining QoE through the optimization problem of max-min weighted quality. Why the max-min problem addresses the short-term quality improvement, inserted weights are to normalize the short-term quality in order to reduce the fluctuation.

\ignore{we heuristically formulate two alternative problems to fairly maximize users's short-term quality with consideration of long-term quality variation: (a) max-min weighted quality and (b) max-sum utility similar to the approaches introduced in \cite{CYim2011} and \cite{VJosheph2014}.}

One primary problem in cross-layer systems considering video adaptation together with resource allocation lies in the fundamental difference of time scale \cite{Hwang2009}, where video adaptation takes place in much slower pace of GOP duration than the adaptation of resource allocation that in scale of short time scale of channel coherence time. Due to the time scale problem, we pursue the QoE improvement through in a sequential series based on a novel quality-driven resource allocation. 

The quality-driven resource allocation is supposed to (1) exploit the frequency and multi-user diversities (offered by OFDMA) in order to improve the spectral efficiency, and at the same time (2) assign resource to users subject to the QoE goals. Besides, one of the primary challenge of resource allocation problem stems from the challenging synchronization task. As a matter of fact, due to several reasons (e.g. oscillator errors, propagation delay and Doppler shift), there is always a chance for residual offsets in time and frequency between user's signals arriving at the Base Station. The imperfect orthogonality of OFDMA signals then causes the significant degradation to user throughput \cite{Le2014}. To deal with these challenges, we first adopt the approach in our previous paper \cite{Le2014}. In that work, to improve the spectral efficiency, we suggest to use shortened overhead and optimize the resource allocation to maximize the user throughput. Then, we extend that approach to pursue the QoE improvement goal.

% (at the scale of channel coherence time). To facilitate that, we formulate a simple but yet efficient coupling function, which estimates the short-term quality at an intermediate step (i.e. resource allocation ) of the process of transmitting a GOP. In other words, given a GOP $l$ sent by user $m$, at each resource allocation step $k$ of the transmission of GOP $l$, this coupling function computes the intermediate short-term quality of GOP $l$ achieved at the resource allocation step $k$ based on the accumulated amount of data that have been already sent in $t$ steps of GOP $l$. Based on the intermediate quality, the algorithm drives the resource adaptation of the next uplink frame so that the intermediate quality is incrementally improved toward the desired overall quality by the end of the GOP.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Main contribution}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

This paper presents a novel cross-layer algorithm, which jointly optimize the video adaptation together with the resource adaptation in OFDMA, in order to improve QoE of videos streamed in uplink. The key features of the proposed algorithm are described as follows:

\begin{itemize}
\item The improvement of QoE includes not only the short-term quality as most of the work in literature, but also the long-term quality (i.e. the quality variation between GOPs).
\item The algorithm takes into account the practical time-scale problem, where the scale of video adaptation is much larger than one of resource adaptation.
\item The algorithm can deal with time-scale problem by pursue the large-scale goal of QoE improvement in the sequential manner based on a series of quality-driven resource allocation.
\item The novel quality-driven resource allocation utilizes the frequency and multiuser diversities, which are unique features offered by OFDMA, to make the most use of the available bandwidth. 
\item The realistic problem posed by the imperfect synchronization in the OFDMA uplink is also considered. 
\item An optimization for the quality-driven resource allocation is formulated subject to the desired QoE improvement. Especially, through mathematical transformation, we introduce a solvable optimization problem to find the optimal solutions. %instead of heuristic approaches based on Lagrangian derivation. 
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Related works}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The problem of developing cross-layer algorithms jointly optimizing video adaptation and resource adaptation to improve perceived video quality has attracted huge interest in the last decade \ignore{, for example \cite{SShakkottai2003}, \cite{HLuo2010}, \cite{PHWu2011}, \cite{DWang2011},\cite{LHe2012}, \cite{KSeshadrinathan2010}, \cite{MRugelj2014}, \cite{VJoseph2014}, \cite{MSchaar2007}, \cite{Khalek2011}, \cite{Martini2007}, \cite{Fracchia2010}}. However, there are three main limitations in the literature, which are (1) the consideration of the short-term long-term quality (i.e. lack of the long-term quality consideration), (2) the unrealistic assumptions about time-scales, and (3) the lack of thorough exploitation of flexible resource allocation in OFDMA uplink. 

First, due to the natural complexity of the quality assessment, many works on joint APP-PHY optimization for multimedia streaming (e.g. \cite{PHWu2011}, \cite{DWang2011},\cite{LHe2012}, \cite{KSeshadrinathan2010}, \cite{MRugelj2014}, \cite{VJoseph2014}) have concerned only short-term quality, which can be measured based on either QoS- or QoE- oriented matrices (where QoS stands for Quality of Service). As some examples of the QoS usage, while the work in \cite{PHWu2011} relies on mere throughput and packet loss rate to measure the perceived quality, the work in \cite{DWang2011} uses the sum of video distortion rate (over all users) in one GOP for the same purpose.  On the other hand, other works such as \cite{LHe2012} \cite{KSeshadrinathan2010} \cite{MRugelj2014} though utilize objective QoE models but focus only in short term video quality per GOP; the quality variation is not properly investigated. A pioneering work in \cite{VJoseph2014} considers long-term quality together with short term quality and other aspects (e.g. re-buffering rate and cost), however the problem of resource adaptation is rather generous and simple and insufficient to exploit the flexibility of OFDMA to improve the spectral efficiency.

Second, it is commonly assumed  in literature the decision for video adaptation takes place at the same time scale as the resource adaptation, e.g.   
 \cite{HLuo2010} \cite{DWang2011} \ignore{\cite{MSchaar2007}} \cite{Khalek2012}. This assumption is typically limiting, since it is most likely inapplicable in practice \cite{Hwang2009}. As a matter of fact, the smallest practical time scale for video adaptation typically equals one GOP, whose duration takes a value in range of few hundred milliseconds (e.g. 500ms). Meanwhile, in order to exploit the frequency and multiuser diversities, the decision of resource adaptation is limited by the channel coherence time, thus normally conducted in few milliseconds (e.g. 5ms). This time scale problem is taken into account in \cite{MMartini2007} and \cite{RFracchia2010}. However, those works focus only on a general architecture for the multimedia transmission over IP-based networks, and no specific investigation about QoE is specifically discussed.

Finally, most of works in literature assume a perfect synchronization between users as they consider the problem of resource adaptation in OFDMA uplink. However, due to several reasons such as oscillator errors, propagation delay and Doppler shift, there is always a chance for residual offsets \cite{XWang2003}, and then the orthogonality of subcarriers, which are sent simultaneously by multiple users, is damaged at Base Station (BS). Optimizing resource allocation without consideration of these residual offsets might lead to inefficient solutions \cite{Le2013}. On the other hand, due to the natural complexity of the optimization of resource allocation (e.g. integer programming), the frequency and multiuser diversities offered by OFDMA are not properly utilized, e.g. \cite{DWang2011} \cite{VJoseph2014}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Organization of the paper}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The rest of the paper is organized as follows: Section II describes the system model used in this work \textit{few words about 4 main sections}. Section III then presents the cross-layer algorithm in depth. In this section, we formulate ... \textit{coupling function, equation for dynamic resource allocation...}. Besides, we discuss about ... and .... Section IV shows the numerical results and analysis to demonstrate the achievable gains. Concluding remarks are given in Section VII. 

\pagebreak
