\documentclass[conference]{IEEEtran}
\bibliographystyle{IEEEtran}


\include{macros}

\newcommand{\ignore}[1]{}

\begin{document}

\title{Quality Driven RA for Adaptive Video Streaming in OFDMA Uplink}

\author{Hieu Le, Arash Behboodi and Adam Wolisz \\ Department of Telecommunication Systems, Technical University of Berlin \\ \{le, behboodi, wolisz\}@tkn.tu-berlin.de}

\maketitle

\begin{abstract}
We investigate cross layer systems, which jointly exploit the video adaptation of adaptive video streaming and the adaptation of resource allocation (RA) in OFDMA, to improve Quality of Experience (QoE) of multiple streams in uplink. Especially, we introduce the novel paradigm to deal with the time scale problem, where video adaptation takes place in much slower pace than the adaptation of RA. Particularly, we develop a novel cross layer algorithm, which achieves the QoE improvement through a sequential process of a novel quality driven RA. The proposed quality-driven RA (1) exploits the frequency and multi-user diversities to improve spectral efficiency, and at the same time (2) assign resource to users subject to the desired QoE goals. Numerical results show the novel algorithm can significantly improve the QoE of multiple streams as expected.

\end{abstract}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Mobile video traffic is one of the main contributors to the exponential traffic growth in cellular networks \cite{Cisco2014}. Without comprehensive solutions, huge bandwidth demand for video leads to the severe spectrum crunch. Although various solutions such as HetNet and massive MIMO have been proposed to increase user throughput, none of them is designed specifically for video delivery. Particularly, the problems of dynamic interference and throughput variability in such networks raise huge challenges for managing Quality of Experience (QoE) of video streams. Given these challenges, optimizing video delivery together with maximizing spectral efficiency is one of the primary networking problems today.

For the efficient video delivery, the adaptive video streaming technique allows senders to adapt the transmission rate of scalable video streams to match the available throughput. One example is the Dynamic Adaptive Video over HTTP (DASH)) standard \cite{Sodagar2011}. Additionally, Orthogonal Frequency Division Multiple Access (OFDMA) is favorable for its flexible and efficient spectral usage. While high spectral efficiency comes from, primarily, the orthogonality of subcarriers, the flexible resource allocation RA allows system to adapt RA scheme to (1) exploit the multiuser and frequency diversities to further improve the spectral usage, and (2) assign resources to appropriate users subject to the chosen utility function \cite{Le2014}.

Beyond the combining of gains in individual layers, tremendous demand for video also raises a need for efficient network designs. The cross-layer design, where non-adjacent layers can exchange information and collaborate, has attracted a large number of work and has been used for several networking problems \cite{Shakkottai2003}. On the contrary, in the traditional OSI architecture, lack of joint consideration of video adaptation and adaptation of RA tends to cause a discrepancy between the resource supply by OFDMA system and throughput demand by video stream. Appropriate cross-layer systems are then expected to (1) efficiently improve spectral usage of OFDMA and (2) assign resources to proper users subject to QoE.

In this work, we investigate cross layer systems, which jointly optimizes video adaptation and adaptation of resource adaptation of OFDMA with the goal to improve QoE of multiple streams in uplink.

The first challenge stems from the complexity of video quality assessment. Particularly, most of related work (e.g. \cite{DWang2011,PHWu2011,LHe2012}) on optimization for adaptive video streaming consider video quality of video segments in separation and do not properly consider the quality consistency between segments. We refer to that separate quality measurement as short term quality (STQ). In fact, it is shown that the temporal variability of STQ can significantly deteriorate the overall perceived quality (i.e. QoE), also referred as long term quality (LTQ) \cite{VJoseph2014}. Especially, the strong fluctuation can result in a QoE that is worse than the one of a more constant quality video with lower average short term quality \cite{Yim2011}. In this work, the time scale problem of managing the quality consistency of STQ in order to improve the overall QoE is referred as \textbf{TSP1}.

The second challenge to design such cross layer systems lies in the time scale problem (\textbf{TSP2}) \cite{Hwang2009}, where video adaptation takes place in much slower pace (e.g. 500ms) than the adaptation of RA (e.g. 5ms). Due to the high complexity of the challenge, most work in literature such as \cite{HLuo2010,DWang2011} assume the process of RA has the same time scale of video adaptation. That assumption can greatly simplify the design as authors focus on the video delivery, but, on the other hand, it leads to two drawbacks. First, that assumption contradicts the \textit{fast} time-varying channel in cellular networks. Second, the slow adaptation of RA cannot exploit the multiuser and frequency diversities in OFDMA to improve the spectral usage.

In this paper, we introduce a novel design to deal with the time scale problem. First, we address three main factors determining QoE, which are: (1) increasing short-term quality (based on Peak Signal to Noise (PSNR)) per Group of Picture (GOP), (2) reducing the fluctuation of STQ, and (3) the quality fairness between users. Consequently, to deal with \textbf{TSP1}, the objective of long-term QoE is formulated as an optimization problem of STQ for each GOP.

Second, we deal with \textbf{TSP2} by achieving the wanted STQ (i.e. PSNR) through a sequential series based on a novel quality-driven RA. Each RA step takes place during one time span of coherence time to exploit the multiuser and frequency diversities, and thus is able to increase the spectral usage of OFDMA. Moreover, the quality-driven RA is, at the same time, assigns resources to users subject to the QoE goals.

To prove the concept, first, we formulate the optimization problem of video adaptation \textbf{OPT-VID} based on the max-min fairness approach. The problem is then transformed into a series of optimization problem of quality driven RA \textbf{OPT-RA}. In general, in each time span of coherence time, we solve one instance of \textbf{OPT-RA} and, thus, improve STQ incrementally and asymptotically to the wanted PSNR value. Finally, the simulation show the novel cross layer design can significantly improve the long term QoE.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{System Model}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

We consider one cell in isolation using OFDMA, where $M$ users share total $N_{sch}$ subchannels, each of which consists of $G$  orthogonal subcarriers, to stream their videos in uplink.

%%%%%%%%%%%%%%%%%%%%
\subsection{Adaptive Video Streaming}
%%%%%%%%%%%%%%%%%%%%

We model the adaptive video streaming based on the Scalable Video Coding extension of the H.264/AVC standard. It means, user's coded video streams are comprised of hierarchical sub-streams. Consequently, user's video adaptation is realized by the selection of sub-streams to send or, in other words, the adaptation of the average video transmission rate. Roughly speaking, the more sub-streams sent and received, the higher perceived quality. The number of sub-streams that can be sent is dynamically adapted based on, for instance, the channel condition and the quality fairness.

In this work, the quality model is based on the Peak Signal to Noise Ratio (PSNR). Particularly, PSNR $Q_{m}^{l}$ for user $m$ in GOP $l$ takes the form as follows \cite{KSeshadrinathan2010}. Thus quality measurement based on PSNR is considered as STQ, since it measures the quality in an individual GOP only.
\begin{equation}
Q_{m}^{l}=10log_{10}(\frac{255^{2}}{MSE})
\label{eq:PSNR}
\end{equation}

Mean squared error $MSE$ (between the source and the re-constructed video) can be efficiently approximated based on the well-known video rate distortion model introduced in \cite{Stuhlmuller2000}.
\begin{equation}
MSE \equiv D_{e}(R_{e}) = \frac{\theta}{R_{e}-R_{0}}+D_{0} \quad ,
\label{eq:DistortionRate}
\end{equation}

where the rate distortion $D_{e}$ is a function of video coding rate $R_{e}$. Characteristics of user's video such are modeled by three constants denoted by $\theta$, $R_{0}$ and $D_{0}$. Figure \ref{fig:DistortionRate} illustrates the characteristics of video stream through the dependence of PSNR on video coding rate $R_e$. In this example, in comparison with the GOP sent by the first user, the one sent by the second user contains less motion, thus requires lower coding rate for a quality similar. This variety of video characteristics is referred as video diversities. In general, a video with high motion requires higher coding rate than the one with low motion for the same quality (i.e. PSNR). We write $Q_m^{l}=G(R_e)$.

\begin{figure}[h]
\centering
\includegraphics[width=0.2\textwidth]{../../Figures/Illustrations/throughputFairness.eps}
\caption{Illustration of the variety of video charactersitics}
\label{fig:DistortionRate}
\end{figure}

Using mere STQ to measure QoE cannot reflect the degradation of QoE caused by the temporal variability of PSNR values between successive GOPs. Therefore, we adopt \textbf{the QoE index (QID)} introduced in  \cite{VJoseph2014} int order to include not only STQ but also the the consistency of PSNR values between GOPs. The QID has the below form:

\begin{equation}
QID = \sum_{m=1}^{M} \big( \mu_m - var_m \big) \quad ,
\label{eq:QID}
\end{equation}

where $\mu_m$ and $var_m$ are the mean and the variance of PSNR values over all GOPs of video sequence, respectively.

%%%%%%%%%%%
\subsection{OFDMA}
%%%%%%%%%%%

We abstract our OFDMA system based on the IEEE 802.16e standard, and the duplex method is Time Devision Duplexing. The smallest addressable OFDMA resource unit is made of one subchannel in frequency and one uplink frame in time.

Before users send uplink frames to Base Station (BS), a RA scheme, which informs users the resource partitions assigned to them, needs to be defined, normally, by BS. To do that, BS can either adopt static schemes (e.g. Blockwise scheme) or dynamically adapt the assignment to several factors such as channel conditions and user's demands. Based on RA scheme, users modulates their subchannels to stream their videos in uplink to BS.

Users' signals arriving at BS degrade due to path-loss, shadowing loss and multipath loss. We assume a slow-fading channel in uplink, and the channel coherence time equals one uplink frame. Then let $H_{i,m}$ then represent the average channel gain that user $m$ experiences on subchannel $i$ at the moment of consideration. Moreover, the signal arriving at BS is actually constituted by several overlapping individual signals. Due to several reasons such as clock errors, propagation delay, Doppler shift, signals arriving at BS are most probably asynchronous \cite{Tonello2000}. Consequently, the asynchronicity damages the orthogonality and gives rise to Multiple Access Interference (MAI). The precise calculation of MAI is introduced in \cite{Tonello2000}. In general, the Signal to Noise plus Interference Ratio (SINR) $\gamma_{i,m}$ can be computed as follows:

\begin{equation}
\gamma_{i,m}=\frac{P_{i,m}^{TX} H_{i,m}}{\sigma^2 + MAI_{i,m}}=\frac{P_{i,m}^{RX}}{\sigma^2 + \sum_{\forall j \neq i,\forall m' \neq m}MAI_{i,m}^{j,m'}}
\label{eq:SINR}
\end{equation}

where $P^{TX}_{i,m}$ and $P^{RX}_{i,m}$ are the transmit and the received power, $\sigma^2$ denotes the thermal noise power. For simplicity, we assume a static and equal transmit power $P^{TX}$ for all subchannel. Based on the instantaneous SINR $\gamma_{i,m}$, user $m$ chooses an appropriate coding and modulation scheme, i.e. Adaptive Coding and Modulation (ACM), to send data on subchannel $i$ at frame/time $k$ subject to a predefine tolerable error rate $P_{err}$. Let $S$ denote the total number of ACM schemes that users can use. Then, we uses function $F(.)$ to represent ACM schemes by returning the number of bits $b_{i,m}$, that user $m$ sends on subchannel $i$ corresponding to SINR $\gamma_{i,m}$, i.e. $b_{i,m} = F(\gamma_{i,m}, P_{err})$.

%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%
\section{General Cross Layer System}
%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%

We describe the cross layer system of interest, which combines the video system in the Application layer and the OFDMA transmission system in lower layers. In general, the main features of the system are described as follows:

\begin{itemize}
\item Video sources are coded into scalable video streams, and the characteristics (i.e. $\theta$, $R_0$ and $D_0$) of all coded GOPs are available for the video adaptation purpose. We assume users' video streams have the same number of GOP, all users' GOPs have an equal and static duration $T^{GOP}$, and transmission of GOPs by users in uplink are synchronous.

\item When a GOP is scheduled to be sent, its video packages are then fed to the transmission queue of the OFDMA system. Especially, video packages are prioritized based on the significance of sub-streams. The prioritization is necessary to assure the decoder can achieve the lowest perceived quality first, then incrementally increase the quality as it receives more packages later.

\item Based on system conditions, BS defines the RA scheme, which can be either static or adaptive. Based on RA scheme, users take resource and send video packages in the queue. Clearly, RA schemes are supposed to distribute resource to users so that they can achieve, first, the wanted STQ with respect to long term QoE. In other words, while sending a GOP, its video packages are selected to send based on the video adaptation decision in order to increase QoE. Video packages of each scheduled GOP are discarded by the end of that GOP. In this work, video packages can assumably be segmented arbitrarily small and fed to MAC frames.

\end{itemize}

%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%
\section{Time Scale Problem}
\label{sec:TimeScaleProblem}
%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%
\subsection{Time Scale Problem 1}
%%%%%%%%%%%%%%%%%%%

In this work, we target the long-term QoE, which essentially include not only STQ, but also the quality consistency. Moreover, a quality fairness between users should also be taken into account. To improve long term QoE, we consider to optimize directly the QoE index \textbf{QID}. However, due to the quadratic form of variance, such approach then leads to severe mathematical challenge. Additionally, QID clearly cannot include the quality fairness.

Therefore, we improve QoE based on the max-min fairness. Particularly, we formulate the optimization problem to maximize the minimum weighted PSNR. While the max-min optimization can encompass the quality fairness and the improvement of STQ, the PSNR values are weighted by the average of achieved PSNR values (over all transmitted GOPs) to reduce the PSNR fluctuation. We refer to this max-min weighted problem as \textbf{OPT-VID}. One instance of \textbf{OPT-VID} is formulated for one group of GOPs that being sent by users at the moment of consideration.

\begin{equation}
\centering
\begin{split}
&\max \quad \Phi \\
s.t. \quad &\frac{Q_m^{l}}{\mu_m} \geq \phi \qquad ,\forall m
\end{split}
\label{eq:simple_maxminQ}
\end{equation}
where $\mu_m = 1/l \sum_{i=1}^{l} Q_m^{l - i}$ is the average PSNR achieved over all transmitted GOPs.

%%%%%%%%%%%%%%%%%%%
\subsection{Time Scale Problem 2}
%%%%%%%%%%%%%%%%%%%

The primary challenge in combining video adaptation and resource adaptation lies in the time scale difference \cite{Hwang2000}.

\begin{itemize}
\item \textbf{Long term video adaptation:} The decision of video adaptation (at APP layer) takes place at the interval of long duration, e.g. GOP length, whose typical value is few hundred milliseconds long (e.g. 500ms).
\item \textbf{Short term resource adaptation:} On the other hand, in order to exploit the frequency and multiuser diversities and thus improve the spectral efficiency, the decision of RA (at MAC) needs to occur at the pace equal the coherence time, which holds for few milliseconds in wireless networks (e.g. 5ms).
\end{itemize}

\begin{figure}[h]
\centering
\includegraphics[width=0.4\textwidth]{../../Figures/Illustrations/TimeScaleProblem_Full2.eps}
\caption{Illustration of the time scale difference}
\label{fig:TimeScaleProblem}
\end{figure}

Figure \ref{fig:TimeScaleProblem} illustrates the time scale difference of video adaptation and RA. In general, one GOP is transmitted in a large number $K$ of OFDMA frames. During each OFDMA frame $k$ in GOP $l$, user $m$ takes some resources and sends $b_m^{k}$ $(0<k<K)$ bytes data. Total amount of data $B_m^{l}$ user $m$ sends in the GOP $l$ is accumulated in all uplink frame $k$ in GOP $l$ and takes the form: $B_m^{l} = \sum_{k=0}^{K-1} b_m^{k}$. Then we have $Q_m^{l} = G(B_m^{l}) = G(\sum_{k=0}^{K-1} b_m^{k})$.

It means, quality values $Q_m^{l}$ need to be achieved through a sequential process of transmission of $K$ uplink frames. Unfortunately, since the channel condition cannot be efficiently predicted for the long GOP duration, it is infeasible to estimate the total amount of data $B_m^{l}$ sent for GOP $l$ before actually sending GOP $l$. Therefore, it is not reasonable to search for the optimal decision of video adaptation at the beginning of GOP as commonly implemented in the literature. On the other hand, optimizing RA without consideration of user's QoE most likely leads to a discrepancy between the supply and the user demand of throughput.

Therefore, a good cross-layer algorithm, which jointly optimize both video adaptation and resource adaptation in order to improve user's QoE, is supposed to work at two levels at the same time. In other words, the algorithm needs to couple the problem of RA (occurring in each small span of coherence time) to the one of the video adaptation (occurring in large duration).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Novel Cross Layer Design}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In this section, we describe a novel paradigm based on the sequential process of quality RA to cope with the time scale problem and improve QoE.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Sequential Process of Quality Driven RA}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Let assume users are now sending uplink frame $k$ of GOP $l$. At this moment, only channel condition for the coming uplink frame $k$ is known. Before the current uplink frame, user $m$ has sent an accumulated data amount of $\tilde{b}_m^{k-1}$. The algorithm currently needs to define the RA, based on which users will takes resources, send a data amount of $b_m^{k}$. Then, we write: $\tilde{b}_m^{k} = \tilde{b}_m^{k-1} + b_m^{k}$. Corresponding to the accumulated amount of transmitted data $\tilde{b}_m^{k}$, we then estimate the intermediate quality $q_m^{k}$  following: $q_m^{k}= G(\tilde{b}_m^{k}) = q_m^{k-1} + g(b_m^{k})$.

It is important to note that, function $g(.)$ essentially estimates the intermediate quality achieved in middle uplink frames of the currently-being-sent GOP. Based on $g(.)$, one can drive the resource adaptation of the every uplink frame, so that the intermediate quality is incrementally improved toward the desired  quality $Q_m^{l}$ by the end of the GOP.

\begin{figure}[h]
\centering
\includegraphics[width=0.28\textwidth]{../../Figures/Illustrations/exBitrateVsQuality_small.jpg}
\caption{Illustration of the sequential process}
\label{fig:intermediateQuality}
\end{figure}

In this work, we derive the simple but yet sufficient formulation of the coupling function $g(.)$. First, based on the definition, we have

\begin{equation}
Q_m^l =10log_{10}(255^2) - 10log_{10}(\frac{\theta}{R_e - R_0} + D_0) \\
\end{equation}
then we take the derivative of PSNR $Q_m^l$ to $R_e$
\begin{equation}
\frac{\delta Q_m^l}{\delta R_e} = \frac{10\theta}{ln(10)} \times \frac{1}{(\theta+D_0(R_e - R_0))(R_e - R_0)}
\label{eq:derivativePSNR}
\end{equation}

And since one GOP typically consists of a large number of uplink frames, the PSNR gain in each uplink frame is relatively small to the overall PSNR value for the GOP. Hence, it is reasonable to approximate $\delta R_e \approx c_m^{k}=b_m^{k}/T^{GOP} $. Consequently, we have
\begin{equation}
\delta Q_m^l \approx q_m^{k} - q_m^{k-1} = \Delta q_m^{k} = g(b_m^{k})
\end{equation}

Consequently, we have

\begin{equation}
g(b_m^{k_m^*,l}) = \frac{10\theta}{ln(10)} \frac{ c_{m}^{k}}{(\theta+D_0(\tilde{c}_{m}^{k-1} - R_0))(\tilde{c}_{m}^{k-1} - R_0)}
\label{eq:exact_g}
\end{equation}

where $\tilde{c}_m^{k}=\tilde{b}_m^{k}/T_m^{GOP}$. Note that, at the uplink frame $k_m^*$, variables $\tilde{c}_m^{k}$ are all defined. And three parameters $\theta$, $D_0$ and $R_0$ are assumably known as described before. Thus, for simplicity, we can write:

\begin{equation}
\tilde{q}_{m}^{k}=\tilde{q}_{m}^{k-1} + \alpha c_{m}^{k}
\label{eq:exact_g}
\end{equation}
 where the constant $\alpha$ takes the following form
\begin{equation}
\alpha = \frac{10\theta}{ln(10)} \frac{1}{(\theta+D_0(\tilde{c}_{m}^{k-} - R_0))(\tilde{c}_{m}^{k-1} - R_0)} \quad .
\end{equation}

Function $g(.)$ becomes a very simple linear function of the intermediate throughput $c_{m}^{k}$. The simplicity of $g(.)$ definitely provides a huge computational benefits as we design the cross layer algorithm of interest.

%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Dynamic Resource Allocation}
%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%

We continue the work by considering the problem of RA in order to develop the appropriate quality-driven RA. Particularly, we adopt the approach introduced in our previous paper \cite{Le2014}. That approach can (1) efficiently exploit the multi-user and frequency diversities in  order to increase the spectral efficiency, and, at the same time, (2) dynamically adapt the RA to improve user throughput. In this work, we then extend that approach to include the QoE goal by using the novel function $g(.)$.

\begin{figure}[ht]
\centering
\includegraphics[width=0.22\textwidth]{../../Figures/Illustrations/SINR_Bit_small.jpg}
\caption{Adaptive Coding and Modulation function $F(.)$}
\label{fig:ACM}
\end{figure}

The key point of the RA is the selection of the modulation and coding schemes, which is represented by $F(.)$. Let  binary variables $x_{i,u}$ represent the RA, $x_{i,u}$ takes 1 if user $u$ takes subchannel $i$ and 0 if not. Consequently, we have $b_m^{k} = \sum_{i=0}^{N_{SCh}-1} x_{i,m} F(\gamma_{i,m}, P_{err})$ is the amount of data user $m$  sends in uplink frame $k$.

Essentially, function $F(.)$ is non-linear, non-continuous and thus non-convex as illustrated in Figure \ref{fig:ACM}, where $Th_{s}$ and $R_{s}$ are the required SINR and the number of bits sent on one subchannel $i$, when the ACM scheme $s$ is chosen from total $S$ available ACM schemes, respectively. Fortunately, similarly as done in \cite{Le2015}, by exploiting the piece-wise linear function \cite{Lee2012}, the problem of RA can be seen as a set of linear and continuous function as follows.

\begin{equation}
\centering
\begin{split}
& (1) b_m^{k} =  \sum_{i} x_{i,m} \Omega_{i,m} \quad (2) \Omega_{i,m} \leq \sum_{s=0}^{S} z_{i,m,s} R_{s} , \forall i,m \\
& (3) \sum_{s=0}^{S} z_{i,m,s} \widetilde{Th}_{i,m,s} \geq \big( \frac{MAI_{i,m}}{\sigma^2} x_{j,m'} \big) \quad , \forall i,m \\
& (4) \sum_{s=0}^{S} z_{i,m,s} = 1 \quad , \forall i,m
\end{split}
\label{eq:novelOP}
\end{equation}

In \eqref {eq:novelOP}, $\Omega_{i,m}$, $z_{i,m,s}$ are auxiliary optimization variables needed for the transformation. Particularly, $\Omega_{i,m}$ is the number of bits user $m$ sends on subchannel $i$, and $z_{i,m,s}$ takes "1" if scheme $s$ is used in subchannel $i$ of user $m$. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Novel Quality Driven Resource Allocation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%


To deal with \textbf{TSP2}, we pursue the optimization problem of \textbf{OPT-VID} in a series of optimization problem of quality-driven RA, referred as \textbf{OPT-RA}. At the beginning of every uplink frame, one instance of OPT-RA is formulated and solved to find the appropriate RA scheme, which can improve the intermediate quality toward the desired quality value. The general formulation of OPT-RA is described as follows

\begin{equation}
\centering
\begin{split}
&\max \quad \Phi \\
s.t. \quad &\frac{q_m^{l}}{\mu_m} \geq \phi \qquad ,\forall m
\end{split}
\label{eq:simple_maxminq}
\end{equation}
We combine the introduced coupling function $g(.)$ and the equivalent transformation of function $F(.)$ above to formulate the exact formulation of \textbf{OPT-RA} as shown in \eqref{eq:maxminq}.

\begin{equation}
\begin{split}
max & \quad \epsilon \\
s.t. \quad &a) \frac{1}{\mu_m} (q_{m}^{k-1} + \frac{\alpha_m^{l}}{T_m^{GOP}} \sum_{i=0}^{N_{sch}-1} x_{i,m} \Omega_{i,m}) \geq \epsilon, \qquad \forall m \\
&b) \Omega_{i,m} \leq \sum_{k=0}^{K} z_{i,m,s} R_{s} \quad, \forall i,m \\
&c) \sum_{k=0}^{K} z_{i,m,s} \widetilde{Th}_{i,m,k} \geq \big( \sum_{j} \sum_{u'} \frac{MAI_{i,m}^{j,m'}}{\sigma^2} x_{j,m'} \big) \quad , \forall i,m \\
&d) \sum_{k=0}^{K} z_{i,m,s} = 1 \quad , \forall i,m \\
&e) \sum_m x_{i,m} \leq 1, \qquad \forall i ,
\end{split}
\label{eq:maxminq}
\end{equation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Evaluation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
We use the Monte Carlo method to evaluate the performance of the proposed cross-layer algorithm and baselines. Particularly, we develop a simulation based on the network simulation framework OMNeT++ and using the optimization solver Gurobi. In total, we simulate three other solutions as baselines.
\begin{itemize}

\item{Static RA + Rate Adapting:} RA and video adaptation run independently. The static RA scheme is based on the block-wise assignment where each user takes an equal and continuous block from the available bandwidth. The video transmission rate is equal the achieved throughput (thus named Rate Adapting).

\item {Adaptive RA + Rate Adapting:} RA and video adaptation do not collaborate. This solution deploys the dynamic RA in \cite{Le2014}, which maximizes minimum user throughput without considering how much user really benefits from the given throughput (i.e. throughput-driven RA). The video data transmission rate is then matched to the available throughput. By comparing with this solution, we investigate the benefit of the cross-layer design.

\item {Proportional-Fairness RA + Rate Adapting:} For a comparison with other works in literature, the solution in \cite{VJoseph2014} might be the most appropriate. However, since the main focus of that work associates with several complex aspects of video delivery and the RA is generous, we cannot directly compare out work with it. Therefore, we choose to compare with the baseline based on the proportional fairness used in that work. Particularly, the corresponding optimization is described as follows
\begin{equation}
max \sum \frac{1}{\rho_m} x_{i,m} F(\gamma_{i,m},P_{err})
\end{equation}
where $\rho_m$ is the peak throughput achieved when user $m$ takes all resources. The video data transmission rate is independently matched to the available resource.

\end{itemize}

For each RA step, the same profile of the system condition is fed to all simulated solutions. Based on the RA at the output of solutions, the simulation computes the SINR of subchannels, selects the appropriate ACM schemes, computes the total amount of data users send (i.e. $b_m^{k_m,l}$). sAfter the final uplink frame of the GOP that has been sent, the video quality value (i.e. PSNR) of GOP is computed based on the video data transmission rate of that GOP, i.e. $Q_m^{l}=G(\sum_{k_m} b_m^{k_m,l})$.

We assume there are 4 users are streaming videos in uplink. Each video streams consists of 30 GOPs, and each GOP in its turn includes 25 OFDMA uplink frames, i.e. $K_m=25, \forall m$. To simulate the GOP's characteristics, we adopt the distortion rate curves introduced in \cite{DWang2011}. In total, we use 4 different video characteristic profiles to simulate user's GOPs. We illustrate these data profiles in Figure \ref{fig:RDCurves}.

\begin{figure}[h]
\centering
\includegraphics[width=0.25\textwidth]{../../Figures/Illustrations/DR.eps}
\caption{Video Rate Distortion Curves}
\label{fig:RDCurves}
\end{figure}

Furthermore, to simulate the change of characteristics between GOPs in user's video sequence, we adopt the following policy. At the beginning, 4 GOPs sent by 4 users have the characteristics following 4 different data profiles, particularly user $m$ takes profile $m$. After an interval of 5 GOPs,  each user takes the profile with the index increased by 1, i.e. user $m$ now takes profile $m+1$ and, especially user 4 now takes the profile 1. The process continues similarly until the end of video sequences. The main parameters for the OFDMA system are listed in Table \ref{tab:simPar}.
\begin{table}[h]
\caption{Simulation parameters for channel.}
\centering
\begin{tabular}{|l|l|}
\hline
Parameters & Values \\ 
\hline
% System
Number of subcarriers & $512$ \\
Number of subchannels & $8$ \\
%Subcarrier spacing & $10940Hz$ \\
Number of subcarriers per subchannel & $64$ \\
Power per subcarrier & $1mW$ \\
\hline
MS maximal time offset & $2 \mu s$ \\
MS maximal frequency offset & $200Hz$ \\
\hline
% Channel
Cell radius & $100 m$ \\
%Path loss model & COST231 Walfish-Ikegami \\
%Log normal shadowing std. dev. & $10 dB$ \\
%Multipath fading model & Jakes's/Clarke's model \\
%Thermal noise density & $-174 dBm/Hz$ \\
%Penetration and other losses & $10 dB$ \\
%Receive Antenna Gain & $14 dB$ \\
%Delay spread (rms) & $0.251 \mu s$ \\
\hline

%\hline
%$v_{2}$ & equal the maximal delay spread \\
%$v_{1}$ &  0 $\mu s$ \\
\hline
\end{tabular}
\label{tab:simPar}
\end{table}

Total 30 runs are conducted for each solution's simulation. For each run, at the beginning, users' locations are defined in the way that their distances to BS in cell are uniformly distributed. During the process of video streaming, users' mobility is modeled based on the Manhattan Mobility Grid. In each run, 750 optimization instances (for 30 GOPs times 25 uplink frames) times 4 solutions are formulated and solved.

We compute the QoE index to measure the user's QoE. Figure \ref{fig:qid} show the average of QoE over all 120 simulated video streams. As it shows, the novel cross-layer algorithm significantly improves the QoE, particularly by about 75\% and 40\% in comparison to those of the \textbf{(MMT+RA)} and \textbf{(PF+RA)}.

\begin{figure}[h]
\centering
\includegraphics[width=0.3\textwidth]{../../Figures/Results/Analysis/qoe.eps}
\caption{QoE Index (QID)}
\label{fig:qid}
\end{figure}

Next, we analyze in details PSNR values achieved to find out what is the cause for the QoE improvement. To demonstrate all results, we show in Figure \ref{fig:cdfRQ} the cumulative distribution function (CDF) of all PSNR values collected for 3600 GOPs  simulated (30 runs times 4 users times 30 GOPs) of each solution. Through the CDF, we show the quality variability as well as the dependence of the solution's performance to the topology. As it shows, the novel approach clearly outperforms others.

\begin{figure}[h]
\centering
\includegraphics[width=0.25\textwidth]{../../Figures/Results/Analysis/cdfQ.eps}
\caption{CDF of PSNR values}
\label{fig:cdfRQ}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[width=0.25\textwidth]{../../Figures/Results/Analysis/meanQ_withErr.eps}
\caption{Average of PSNR values}
\label{fig:meanRQ}
\end{figure}

We show in Figure \ref{fig:meanRQ} the average of PSNR achieved in different solutions. As it shows, the cross-layer algorithm cannot provide a higher PSNR average. Actually, the proportional fairness and max-min user throughput, which run independently from the video adaptation, can provide better avg. PSNR. This can be explained by the nature of the max-min fairness, where the worse user essentially limits the whole system performance.

Therefore, the main cause for the QoE improvement is the efficient reduction of the PSNR fluctuation between GOPs. In the other words, the novel cross layer algorithm can match the RA efficiently to the actuaa demand of users, as the user's streams have different characteristics from GOP to GOP. More interestingly, as shown in the Appendix, our novel cross layer algorithm can perform independently from the topology. The PSNR values for all users in all 30 runs vary around $25dB$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusion}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In this work, we introduce a novel paradigm to design the cross layer system, which jointly exploit the video adaptation of adaptive video streaming and the RA adaptation of OFDMA, with the goal to improve Quality of Experience (QoE) of multiple video streams in uplink. Specifically, our objective is to increase QoE in three aspects, which are (1) increasing short term quality per Group of Pictures (GOP), (2) reducing the variation of short term qualities between adjacent GOPs, and (3) taking into account a quality fairness among users. In order to deal with the unavoidable problem of time-scale difference, where the time-scale for video adaptation is one GOP and equal several channel coherence time spans, we pursue the QoE improvement through a sequential series of short-scale quality driven RA. The proposed quality-driven RA (1) exploits the frequency and multi-user diversities (offered by OFDMA) in order to improve the spectral efficiency, and at the same time (2) assign resource to users subject to the QoE goals. Further, we extend the proposed cross-layer algorithm with different approaches for improving QoE.

% ================================================================
\bibliography{project3.bib}
% ================================================================

\end{document}
