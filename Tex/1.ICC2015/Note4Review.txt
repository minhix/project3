1.
Aristomenopoulos,"A Novel Framework for Dynamic Utility-Based QoE Provisioning in Wireless Network",Globecom 2010.

+ Extension from QoS to QoE, which is defined dynamically based on instantaneous experience.

+ NUM: network utility maximization

+ QoE->QoS->resource allocation

+ CDMA.

2.
Amendment 1: New Appendix I – Definition of
Quality of Experience (QoE)

Quality of Experience (QoE)
The overall acceptability of an application or service, as perceived subjectively by the end-user.
NOTE 1 – Quality of Experience includes the complete end-to-end system effects (client, terminal, network,
services infrastructure, etc.).
NOTE 2 – Overall acceptability may be influenced by user expectations and context. 
