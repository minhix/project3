%ANALYSIS 2

% Convert from cell to matrix

    q0=cell2mat(Q0); r0=cell2mat(R0);
    q1=cell2mat(Q1); r1=cell2mat(R1);
    q6=cell2mat(Q6); r6=cell2mat(R6);
    q7=cell2mat(Q7); r7=cell2mat(R7);
    q8=cell2mat(Q8); r8=cell2mat(R8);

% Plot the cdf of all population of 3600 PSNR values for each solution
% (30run*4user*30GOPs = 3600 points)

    allrunQ=zeros(5,3600); allrunR=zeros(5,3600);

    for r=1:30
        for u=1:4
            for g=1:30
                allrunR(1,(r-1)*30*4 + (u-1)*30 + g)=r0(r,u,g);
                allrunR(2,(r-1)*30*4 + (u-1)*30 + g)=r1(r,u,g);
                allrunR(3,(r-1)*30*4 + (u-1)*30 + g)=r6(r,u,g);
                allrunR(4,(r-1)*30*4 + (u-1)*30 + g)=r7(r,u,g);
                allrunR(5,(r-1)*30*4 + (u-1)*30 + g)=r8(r,u,g);
                
                allrunQ(1,(r-1)*30*4 + (u-1)*30 + g)=q0(r,u,g);
                allrunQ(2,(r-1)*30*4 + (u-1)*30 + g)=q1(r,u,g);
                allrunQ(3,(r-1)*30*4 + (u-1)*30 + g)=q6(r,u,g);
                allrunQ(4,(r-1)*30*4 + (u-1)*30 + g)=q7(r,u,g);
                allrunQ(5,(r-1)*30*4 + (u-1)*30 + g)=q8(r,u,g);
            end
        end
    end
    
    %Plot mean and std for Rate
        bar(mean(allrunR(1:4,:),2)); grid on; set(gca,'FontSize',16);

        bar(std(transpose(allrunR(1:4,:)))); grid on; set(gca,'FontSize',16);
        
        barwitherr(std(transpose(allrunR(1:4,:))),transpose(mean(allrunR(1:4,:),2)));
        
    %Plot mean and std for Quality
        bar(mean(allrunQ(1:4,:),2)); grid on; set(gca,'FontSize',16);
        
        bar(std(transpose(allrunQ(1:4,:)))); grid on; set(gca,'FontSize',16);
        
        barwitherr(std(transpose(allrunQ(1:4,:))),transpose(mean(allrunQ(1:4,:),2)));
        
% QoE Index = sum_over_all_user meanQ - varianceQ
        qoe=zeros(1,5); qoe_err=zeros(1,5);
        
        for r=1:30
            for u=1:4
                qoe(1,1) = qoe(1,1)+1/30*(mean(q0(r,u,:)) - std(q0(r,u,:))*std(q0(r,u,:)));
                qoe(1,2) = qoe(1,2)+1/30*(mean(q1(r,u,:)) - std(q1(r,u,:))*std(q1(r,u,:)));
                qoe(1,3) = qoe(1,3)+1/30*(mean(q6(r,u,:)) - std(q6(r,u,:))*std(q6(r,u,:)));
                qoe(1,4) = qoe(1,4)+1/30*(mean(q7(r,u,:)) - std(q7(r,u,:))*std(q7(r,u,:)));
                qoe(1,5) = qoe(1,5)+1/30*(mean(q8(r,u,:)) - std(q8(r,u,:))*std(q8(r,u,:)));
            end
        end
q11(1,:)= [26.5964 24.9282 25.3895 25.9434 25.1387 26.0167 25.0069 25.3169 25.2412 25.4649 19.6889 19.4206 19.2801 19.2801 19.5567 30.5457 30.5018 30.7173 30.5891 29.6366 26.2397 25.2406 25.2907 25.2406 25.2406 25.8168 25.0069 25.5373 25.6087 25.5012 ];
q11(2,:)= [24.4124 25.2029 25.6087 26.1138 25.3915 21.1256 18.9852 19.2082 19.0608 19.4206 31.4238 30.2301 30.1361 30.3224 30.5457 25.8995 25.3895 25.7202 25.534 24.9282 23.9877 25.5012 25.3169 25.4649 25.3543 22.1016 18.9084 19.2082 19.6889 19.2801 ];
q11(3,:)= [21.5758 20.1229 19.3509 19.8172 19.1351 30.964 29.6366 30.0402 29.8426 30.1361 25.8553 25.2907 25.3404 25.3404 25.4863 25.6087 25.644 25.8507 25.7827 25.0468 20.9814 19.3509 19.1351 19.3509 19.2801 31.4238 29.5836 30.1361 30.5457 30.1361 ];
q11(4,:)= [29.5836 29.8426 30.3678 31.0437 29.9916 25.534 24.764 25.2907 24.9817 25.1899 25.1255 25.5373 25.3543 25.4283 25.6087 21.3111 19.4206 19.6233 19.5567 18.9084 30.6322 30.2301 30.0884 30.0884 30.1361 26.1149 24.764 25.2907 25.5812 25.1899 ];
    