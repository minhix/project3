

tempR=zeros(4,30);
mylegend = cell(1,4);
mylegend(1)={'User 1'};mylegend(2)={'User 2'};mylegend(3)={'User 3'};mylegend(4)={'User 4'};

%mylegend(5)={'Location'};mylegend(6)={'northwest'};

x=1:1:30;

% Method: Blockwise
for run=1:30
    for u=1:4
        for gop=1:30
            tempR(u,gop)=cell2mat(R0(run,u,gop));
        end
    end
     
    fig0 = figure(run);
    
    h=plot(x,tempR(1,:),'-r',x,tempR(2,:),'--g',x,tempR(3,:),':b',x,tempR(4,:),'-.c','LineWidth',2);
    
    ylim([0.25 1.55]);
    grid on;
    xlabel('GOP [index]','FontSize',16);
    ylabel('Throughput (x10e5[bps]','FontSize',16);
    title(sprintf('Method= %01d, RUN = %02d, ',0,run));
    
    legend(h,mylegend,'Location','northwest');
    
    print(sprintf('R%01d_%02d',0,run),'-depsc');close(fig0);
    
end

% Method: OP1
for run=1:30
    for u=1:4
        for gop=1:30
            tempR(u,gop)=cell2mat(R1(run,u,gop));
        end
    end
    
    fig1=figure(run);
    
    h=plot(x,tempR(1,:),'-r',x,tempR(2,:),'--g',x,tempR(3,:),':b',x,tempR(4,:),'-.c','LineWidth',2);
    
    ylim([0.25 1.55]);
    grid on;
    xlabel('GOP [index]','FontSize',16);
    ylabel('Throughput (x10e5[bps]','FontSize',16);
    title(sprintf('Method= %02d, RUN = %02d, ',1,run));
    
    legend(h,mylegend,'Location','northwest');
    
    print(sprintf('R%01d_%02d',1,run),'-depsc');close(fig1);
    
end

% Method: OP6
for run=1:30
    for u=1:4
        for gop=1:30
            tempR(u,gop)=cell2mat(R6(run,u,gop));
        end
    end
    
    fig6=figure(run);
    
    h=plot(x,tempR(1,:),'-r',x,tempR(2,:),'--g',x,tempR(3,:),':b',x,tempR(4,:),'-.c','LineWidth',2);
    
    ylim([0.25 1.55]);
    grid on;
    xlabel('GOP [index]','FontSize',16);
    ylabel('Throughput (x10e5[bps]','FontSize',16);
    title(sprintf('Method= %02d, RUN = %02d, ',6,run));
    
    legend(h,mylegend,'Location','northwest');
    
    print(sprintf('R%01d_%02d',6,run),'-depsc');close(fig6);
    
end

% Method: OP7
for run=1:30
    for u=1:4
        for gop=1:30
            tempR(u,gop)=cell2mat(R7(run,u,gop));
        end
    end
    
    fig7=figure(run);
    
    h=plot(x,tempR(1,:),'-r',x,tempR(2,:),'--g',x,tempR(3,:),':b',x,tempR(4,:),'-.c','LineWidth',2);
    
    ylim([0.25 1.55]);
    grid on;
    xlabel('GOP [index]','FontSize',16);
    ylabel('Throughput (x10e5[bps]','FontSize',16);
    title(sprintf('Method= %02d, RUN = %02d, ',7,run));
    
    legend(h,mylegend,'Location','northwest');
    
    print(sprintf('R%01d_%02d',7,run),'-depsc');close(fig7);
    
end

% Method: OP8
for run=1:30
    for u=1:4
        for gop=1:30
            tempR(u,gop)=cell2mat(R8(run,u,gop));
        end
    end
    
    fig8=figure(run);
    
    h=plot(x,tempR(1,:),'-r',x,tempR(2,:),'--g',x,tempR(3,:),':b',x,tempR(4,:),'-.c','LineWidth',2);
    
    ylim([0.25 1.55]);
    grid on;
    xlabel('GOP [index]','FontSize',16);
    ylabel('Throughput (x10e5[bps]','FontSize',16);
    title(sprintf('Method= %02d, RUN = %02d, ',8,run));
    
    legend(h,mylegend,'Location','northwest');
    
    print(sprintf('R%01d_%02d',8,run),'-depsc');close(fig8);
    
end