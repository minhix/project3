%ANALYSIS 2

% Convert from cell to matrix

    q0=cell2mat(Q0); r0=cell2mat(R0);
    q1=cell2mat(Q1); r1=cell2mat(R1);
    q6=cell2mat(Q6); r6=cell2mat(R6);
    q7=cell2mat(Q7); r7=cell2mat(R7);
    q8=cell2mat(Q8); r8=cell2mat(R8);

% Plot the cdf of all population of 3600 PSNR values for each solution
% (30run*4user*30GOPs = 3600 points)

    allrunQ=zeros(5,3600); allrunR=zeros(5,3600);

    for r=1:30
        for u=1:4
            for g=1:30
                allrunR(1,(r-1)*30*4 + (u-1)*30 + g)=r0(r,u,g);
                allrunR(2,(r-1)*30*4 + (u-1)*30 + g)=r1(r,u,g);
                allrunR(3,(r-1)*30*4 + (u-1)*30 + g)=r6(r,u,g);
                allrunR(4,(r-1)*30*4 + (u-1)*30 + g)=r7(r,u,g);
                allrunR(5,(r-1)*30*4 + (u-1)*30 + g)=r8(r,u,g);
                
                allrunQ(1,(r-1)*30*4 + (u-1)*30 + g)=q0(r,u,g);
                allrunQ(2,(r-1)*30*4 + (u-1)*30 + g)=q1(r,u,g);
                allrunQ(3,(r-1)*30*4 + (u-1)*30 + g)=q6(r,u,g);
                allrunQ(4,(r-1)*30*4 + (u-1)*30 + g)=q7(r,u,g);
                allrunQ(5,(r-1)*30*4 + (u-1)*30 + g)=q8(r,u,g);
            end
        end
    end
    
    %Plot mean and std for Rate
        bar(mean(allrunR(1:4,:),2)); grid on; set(gca,'FontSize',16);

        bar(std(transpose(allrunR(1:4,:)))); grid on; set(gca,'FontSize',16);
        
    %Plot mean and std for Quality
        bar(mean(allrunQ(1:4,:),2));
        
        bar(std(transpose(allrunQ(1:4,:))));

    