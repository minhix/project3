%Convert the indeces
    U=zeros(3,30,4,100);
    for sol=1:3
        for run=1:30
            for u=1:4
                for gop=1:100
                    U(sol,run,u,gop)=gopU(sol,run,gop,u);
                end
            end
        end
    end
    
%Calculate the U variation, i.e. jitter
    jitter=zeros(3,30,4,99);
    for sol=1:3
        for run=1:30
            for u=1:4
                for gop=1:99
                    jitter(sol,run,u,gop)=U(sol,run,u,gop+1)-U(sol,run,u,gop);
                end
            end
        end
    end
    
%Aux jitter matrix
    tempj1=zeros(3,30,4*99);
    for sol=1:3
        for run=1:30
            for u=1:4
                for gop=1:99
                    tempj1(sol,run,(u-1)*99+gop)=jitter(sol,run,u,gop);
                end
            end
        end
    end
    
    tempj2=zeros(3,30*4*99);
    for sol=1:3
        for run=1:30
            for u=1:4
                for gop=1:99
                    tempj2(sol,(run-1)*(u-1)*99+gop)=jitter(sol,run,u,gop);
                end
            end
        end
    end
    
%Find the worse utilization value for each GOP.
%Plot the box plot of the set of worse utilization values.
    wu=zeros(3,30,100);
    for sol=1:3
        for run=1:30
            for gop=1:100
                wu(sol,run,gop)=min(U(sol,run,gop));
            end
        end
    end
    
    tempwu=zeros(3,30*100);
    for sol=1:3
        for run=1:30
            for gop=1:100
                tempwu(sol,(run-1)*100+gop)=wu(sol,run,gop);
            end
        end
    end
    
    tempwu2=zeros(3*30*100,1);
    for sol=1:3
        for run=1:30
            for gop=1:100
                tempwu2((sol-1)*30*100+(run-1)*100+gop)=wu(sol,run,gop);
            end
        end
    end
    tempsolid=zeros(3*30*100);
    for sol=1:3
        for run=1:30
            for gop=1:100
                tempsolid((sol-1)*30*100+(run-1)*100+gop)=sol;
            end
        end
    end
    
    tempsolid2=char(3*30*100,20);
    for run=1:30
        for gop=1:100
            sol=1;
            tempsolid((sol-1)*30*100+(run-1)*100+gop)='      Static RA     ';
            sol=2;
            tempsolid((sol-1)*30*100+(run-1)*100+gop)='Throughput driven RA';
            sol=3;
            tempsolid((sol-1)*30*100+(run-1)*100+gop)='  Quality driven RA ';
        end
    end

    
    
                
    
    