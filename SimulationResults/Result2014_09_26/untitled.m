%Convert the indeces
    U=zeros(3,30,4,100);
    for sol=1:3
        for run=1:30
            for u=1:4
                for gop=1:100
                    U(sol,run,u,gop)=gopU(sol,run,gop,u);
                end
            end
        end
    end
    
%Calculate the U variation, i.e. jitter
    jitter=zeros(3,30,4,99);
    for sol=1:3
        for run=1:30
            for u=1:4
                for gop=1:99
                    jitter(sol,run,u,gop)=U(sol,run,u,gop+1)-U(sol,run,u,gop);
                end
            end
        end
    end
    
%Aux jitter matrix
    tempj1=zeros(3,30,4*99);
    for sol=1:3
        for run=1:30
            for u=1:4
                for gop=1:99
                    tempj1(sol,run,(u-1)*99+gop)=jitter(sol,run,u,gop);
                end
            end
        end
    end
    
    tempj2=zeros(3,30*4*99);
    for sol=1:3
        for run=1:30
            for u=1:4
                for gop=1:99
                    tempj2(sol,(run-1)*(u-1)*99+gop)=jitter(sol,run,u,gop);
                end
            end
        end
    end
    
%Find the worse utilization value for each GOP.
%Plot the box plot of the set of worse utilization values.
    wu=zeros(3,30,100);
    for sol=1:3
        for run=1:30
            for gop=1:100
                wu(sol,run,gop)=min(U(sol,run,:,gop));
            end
        end
    end
    
    tempwu=zeros(3,30*100);
    for sol=1:3
        for run=1:30
            for gop=1:100
                tempwu(sol,(run-1)*100+gop)=wu(sol,run,gop);
            end
        end
    end
    
    tempwu2=zeros(3*30*100,1);
    for sol=1:3
        for run=1:30
            for gop=1:100
                tempwu2((sol-1)*30*100+(run-1)*100+gop)=wu(sol,run,gop);
            end
        end
    end
    cdfplot(tempwu(1,:));hold all;
    cdfplot(tempwu(2,:));
    cdfplot(tempwu(3,:));
    
    tempsolid=zeros(3*30*100);
    for sol=1:3
        for run=1:30
            for gop=1:100
                tempsolid((sol-1)*30*100+(run-1)*100+gop)=sol;
            end
        end
    end
    
    tempsolid2=char(3*30*100,20);
    for run=1:30
        for gop=1:100
            sol=1;
            tempsolid((sol-1)*30*100+(run-1)*100+gop)='      Static RA     ';
            sol=2;
            tempsolid((sol-1)*30*100+(run-1)*100+gop)='Throughput driven RA';
            sol=3;
            tempsolid((sol-1)*30*100+(run-1)*100+gop)='  Quality driven RA ';
        end
    end
    
    boxplot(tempwu2,tempsolid);
    
%Plot the mean with confidence interval

    meanU=zeros(1,3);
    error_meanU=zeros(1,3);
    
    [a b c d] = normfit(tempwu(1,:)); meanU(1)=a;error_meanU(1)=c(2)-a;
    [a b c d] = normfit(tempwu(2,:)); meanU(2)=a;error_meanU(2)=c(2)-a;
    [a b c d] = normfit(tempwu(3,:)); meanU(3)=a;error_meanU(3)=c(2)-a;

    barwitherr(error_meanU,meanU);
    grid on;
    set(gca,'FontSize',14,'XTickLabel',['     Static RA      ';'Throughput driven RA';'  Quality driven RA ']);
    ylabel('Avg. worse PSNR values[dB]');

%Plot CDF of the worse PSNR
    jow=zeros(3,30,99);
    for sol=1:3
        for run=1:30
            for gop=1:99
                jow(sol,run,gop)=min(U(sol,run,:,gop+1))-min(U(sol,run,:,gop));
            end
        end
    end
    
    tempjow=zeros(3,30*99);
    for sol=1:3
        for run=1:30
            for gop=1:99
                tempjow(sol,(run-1)*99+gop)=jow(sol,run,gop);
            end
        end
    end
    
    
%Plot an example of everything
    run=1;
    tempU1=zeros(4,100);tempU2=zeros(4,100);tempU3=zeros(4,100);
    for u=1:4
        for gop=1:100
            tempU1(u,gop)=U(1,run,u,gop);
            tempU2(u,gop)=U(2,run,u,gop);
            tempU3(u,gop)=U(3,run,u,gop);
        end
    end
    
    plot(tempU1(1,:),'-xb'); hold all; grid on;
    plot(tempU1(2,:),'-+r');
    plot(tempU1(3,:),'--xk');
    plot(tempU1(4,:),'-og');
    xlabel('GOP','FontSize',14);ylabel('PSNR[dB]','FontSize',14);
    set(gca,'FontSize',14);
    
    plot(tempU2(1,:),'-xb'); hold all; grid on;
    plot(tempU2(2,:),'-+r');
    plot(tempU2(3,:),'--xk');
    plot(tempU2(4,:),'-og');
    xlabel('GOP','FontSize',14);ylabel('PSNR[dB]','FontSize',14);
    set(gca,'FontSize',14);
    
    plot(tempU3(1,:),'-xb'); hold all; grid on;
    plot(tempU3(2,:),'-+r');
    plot(tempU3(3,:),'--xk');
    plot(tempU3(4,:),'-og');
    xlabel('GOP','FontSize',14);ylabel('PSNR[dB]','FontSize',14);
    set(gca,'FontSize',14);
    
    