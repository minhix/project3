%Input: matrix U(sol,run,u,gop)

SOL=3; RUN=30; M=4; GOP=100;

for run=1:RUN
    h=figure;
    title(sprintf('Run %d',run));
    for sol=1:SOL
        subplot(SOL,1,sol);xlabel('PSNR');ylabel('GOP');
        
        tmp=zeros(M,GOP);
        for m=1:M
            for gop=1:GOP
                tmp(m,gop)=U(sol,run,m,gop);
            end
            plot(tmp(m,:));title(sprintf('run=%02d, solution=%02d',run,sol));hold all;
        end
        if sol~=1
            ylim([12 16]);
        end
    end
    text(10,30,sprintf('Run=%d',run));
    saveas(h,sprintf('Figs/gopU_Run%02d.jpg',run));close(h);
end

        
        

    



